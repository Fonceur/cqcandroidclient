package com.charmedquark.cqcandroidc;

import java.util.Vector;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.res.Resources.NotFoundException;

import com.charmedquark.xmlgwclient.*;


public class WeatherActivity extends GWActivity
{
	// -----------------------------------------------------------------------
	//	These are the indices in our field list that we put the current
	//	condition fields into, so that we know where to go to to find them.
	// -----------------------------------------------------------------------	
	final int kFldInd_CurIcon = 0;
	final int kFldInd_CurText = 1;
	final int kFldInd_CurTemp = 2;
	final int kFldInd_CurHumid = 3;
	final int kFldInd_CurBaro = 4;
	final int kFldInd_CurWndSp = 5;
	final int kFldInd_CurFCText = 6;
	
	final int kFldInd_FCD1_High = 7;
	final int kFldInd_FCD1_Icon = 8;
	final int kFldInd_FCD1_Low = 9;
	final int kFldInd_FCD1_Text = 10;
	final int kFldInd_FCD1_Stamp = 11;

	final int kFldInd_FCD2_High = 12;
	final int kFldInd_FCD2_Icon = 13;
	final int kFldInd_FCD2_Low = 14;
	final int kFldInd_FCD2_Text = 15;
	final int kFldInd_FCD2_Stamp = 16;

	final int kFldInd_FCD3_High = 17;
	final int kFldInd_FCD3_Icon = 18;
	final int kFldInd_FCD3_Low = 19;
	final int kFldInd_FCD3_Text = 20;
	final int kFldInd_FCD3_Stamp = 21;	
	
	final int kFldInd_CurFldsCnt = 22;
	
	
	public WeatherActivity()
	{
		super();
		
		// Allocate some vectors for info
		curFlds = new Vector<FldInfo>();
	}
	
	
	// -----------------------------------------------------------------------
	//	Methods we have to override from our common activity base class
    // -----------------------------------------------------------------------
	@Override
	protected int getLayoutResourceId()
	{
		return R.layout.activity_weather;
	}


	// -----------------------------------------------------------------------
	//	Lifecycle methods we have to handle
	// -----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        // Make sure we get an initial thread state transition
        lastThreadState = null;

        // And make sure we get new room config
	    lastRoomSerNum = 0;

	    // Look up our views that we need to update
	    viewCurBaro = (TextView)findViewById(R.id.curBaro);
	    viewCurCondText = (TextView)findViewById(R.id.curCondText);
	    viewCurFCText = (TextView)findViewById(R.id.curForecast);
	    viewCurHumidity = (TextView)findViewById(R.id.curHumidity);
	    viewCurIcon = (ImageView)findViewById(R.id.curCondIcon);
	    viewCurTemp = (TextView)findViewById(R.id.curTemp);
	    viewCurWndSp = (TextView)findViewById(R.id.curWndSpeed);

	    viewFCD1_High = (TextView)findViewById(R.id.fcD1HighTemp);
	    viewFCD1_Icon = (ImageView)findViewById(R.id.fcD1CondIcon);
	    viewFCD1_Low = (TextView)findViewById(R.id.fcD1LowTemp);
	    viewFCD1_Text = (TextView)findViewById(R.id.fcD1CondText);
	    viewFCD1_Stamp = (TextView)findViewById(R.id.fcD1Stamp);
	    
	    viewFCD2_High = (TextView)findViewById(R.id.fcD2HighTemp);
	    viewFCD2_Icon = (ImageView)findViewById(R.id.fcD2CondIcon);
	    viewFCD2_Low = (TextView)findViewById(R.id.fcD2LowTemp);
	    viewFCD2_Text = (TextView)findViewById(R.id.fcD2CondText);
	    viewFCD2_Stamp = (TextView)findViewById(R.id.fcD2Stamp);	    

	    viewFCD3_High = (TextView)findViewById(R.id.fcD3HighTemp);
	    viewFCD3_Icon = (ImageView)findViewById(R.id.fcD3CondIcon);
	    viewFCD3_Low = (TextView)findViewById(R.id.fcD3LowTemp);
	    viewFCD3_Text = (TextView)findViewById(R.id.fcD3CondText);
	    viewFCD3_Stamp = (TextView)findViewById(R.id.fcD3Stamp);
	    
        // Enable our update timer
        enableUpdateTimer(updateTimer);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();

    	//	Do an initial refresh to make sure we are in sync.
    	refreshView();
    }

    @Override
    protected void onStop()
    {
       	super.onStop();
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }

	// -----------------------------------------------------------------------
    // 	An update timer we register with our parent class. It'll get
    //	called periodically so we can update status info.
	// -----------------------------------------------------------------------
    private Runnable updateTimer = new Runnable()
    {
    	public void run()
    	{
    		// Do any updating required
    		refreshView();
    	}
    };

    
	// -----------------------------------------------------------------------
    //	Private helper methods
	// -----------------------------------------------------------------------

    private void updateIcon(int ourInd, FldInfo.FldIndices baseInd, ImageView tarView)
    {
    	FldInfo newVal = checkFieldChange(ourInd, baseInd, curFlds);
		if (newVal != null)
		{
			// Load the image for the new condition number
			String imgName = "weather_" + newVal.getStringVal();
			int resID = getResources().getIdentifier(imgName, "drawable", getPackageName());
			if (resID == 0)
				resID = 99;
			try
			{
				Drawable myIcon = getResources().getDrawable(resID);
				tarView.setImageDrawable(myIcon);
			}
	
			catch(NotFoundException e) {}
		}
    }
    
    
    //
    //	This is called upon creation, if our timer sees that the room
    //	config has changed, or when we are started up again. We see if the
    //	info we have is still valid. If not, we update ourself. WE have to
    //	get the new room list, init our field list, and update our display
    //	appropriately to reflect the loads we have.
    //
    private void refreshView()
    {
    	//
    	//	Get the current state of the bgn thread, and the latest room
    	//	config serial number.
    	//
    	final ThreadStates curState = getState();
    	final int curSerNum = getRoomSerialNum();

    	// If either the thread state or the serial number has changed, see what's up
    	if ((lastThreadState == null)
    	||  (lastThreadState != curState)
    	||  (curSerNum != lastRoomSerNum))
    	{
    		// We can update the thread state either way
    		lastThreadState = curState;
    		
    		//
    		//	If we either are not ready, or we have no room config, we have to
    		//	just disable everything and do nothing.
    		//
    		if ((curState != ThreadStates.Ready) || (curSerNum == -1))
    		{
    			//
	    		// 	At this point, we aren't going to get to the code below,
    			//	so update the last serial number now. Even if it's -1, that's
    			//	fine, that insures we see a change if it goes to anything
    			//	that indicates we have content.
    			//
    			//	We don't want to store it otherwise, since we want to see
    			//	below 
    			//
    			lastRoomSerNum = curSerNum;
    			
    			return;
    		}
    	}
    	
    	//
    	// 	See if our room configuration has changed. The code above handled
    	//	the situation where it was -1 (meaning none available, so we know
    	//	we have some data here.
    	//
    	if (curSerNum != lastRoomSerNum)
    	{
    		//
    		// 	Store the current serial number. Do this first. It's possible
    		//	it's already changed or will before we complete this. This
    		//	insures that we'll at least see another change next time. 
    		//
    		lastRoomSerNum = curSerNum;
    		
    		//
    		//	Set up our field info vector. It initially stores a null value
    		//	so we are sure to see changes below and do an update of
    		//	everything.
    		//
    		//	Do the current condition fields
    		//
    		curFlds.removeAllElements();
    		for (int index = 0; index < kFldInd_CurFldsCnt; index++)
    			curFlds.add(null);

    		// For each forecast day, do slots for each forecast field
    	}
    	
    	// And check for field changes. Update the ones that have changed
		FldInfo newVal;
		
		newVal = checkFieldChange(kFldInd_CurBaro, FldInfo.FldIndices.Weath_CurBaro, curFlds);
		if (newVal != null)
			viewCurBaro.setText(newVal.getFmtFloatVal());

		newVal = checkFieldChange(kFldInd_CurText, FldInfo.FldIndices.Weath_CurCondText, curFlds);
		if (newVal != null)
			viewCurCondText.setText(newVal.getStringVal());

		newVal = checkFieldChange(kFldInd_CurFCText, FldInfo.FldIndices.Weath_CurFCText, curFlds);
		if (newVal != null)
			viewCurFCText.setText(newVal.getStringVal());

		newVal = checkFieldChange(kFldInd_CurHumid, FldInfo.FldIndices.Weath_CurHumidity, curFlds);
		if (newVal != null)
			viewCurHumidity.setText(newVal.getStringVal() + "%");

		updateIcon(kFldInd_CurIcon, FldInfo.FldIndices.Weath_CurIcon, viewCurIcon);
		
		newVal = checkFieldChange(kFldInd_CurTemp, FldInfo.FldIndices.Weath_CurTemp, curFlds);
		if (newVal != null)
			viewCurTemp.setText(newVal.getStringVal());
		
		newVal = checkFieldChange(kFldInd_CurWndSp, FldInfo.FldIndices.Weath_CurWindSpeed, curFlds);
		if (newVal != null)
			viewCurWndSp.setText(newVal.getStringVal());
		

		// Day 1 forecast stuff
		newVal = checkFieldChange(kFldInd_FCD1_Stamp, FldInfo.FldIndices.Weath_FCD1_Stamp, curFlds);
		if (newVal != null)
			viewFCD1_Stamp.setText(GenHelpers.tmToFull(newVal.getStringVal()));
		
		newVal = checkFieldChange(kFldInd_FCD1_Text, FldInfo.FldIndices.Weath_FCD1_CondText, curFlds);
		if (newVal != null)
			viewFCD1_Text.setText(newVal.getStringVal());
		
		updateIcon(kFldInd_FCD1_Icon, FldInfo.FldIndices.Weath_FCD1_Icon, viewFCD1_Icon);
		
		newVal = checkFieldChange(kFldInd_FCD1_Low, FldInfo.FldIndices.Weath_FCD1_Low, curFlds);
		if (newVal != null)
			viewFCD1_Low.setText(newVal.getStringVal());
		
		newVal = checkFieldChange(kFldInd_FCD1_High, FldInfo.FldIndices.Weath_FCD1_High, curFlds);
		if (newVal != null)
			viewFCD1_High.setText(newVal.getStringVal());

		
		// Day 2 forecast stuff
		newVal = checkFieldChange(kFldInd_FCD2_Stamp, FldInfo.FldIndices.Weath_FCD2_Stamp, curFlds);
		if (newVal != null)
			viewFCD2_Stamp.setText(GenHelpers.tmToFull(newVal.getStringVal()));
		
		newVal = checkFieldChange(kFldInd_FCD2_Text, FldInfo.FldIndices.Weath_FCD2_CondText, curFlds);
		if (newVal != null)
			viewFCD2_Text.setText(newVal.getStringVal());
		
		updateIcon(kFldInd_FCD2_Icon, FldInfo.FldIndices.Weath_FCD2_Icon, viewFCD2_Icon);
		
		newVal = checkFieldChange(kFldInd_FCD2_Low, FldInfo.FldIndices.Weath_FCD2_Low, curFlds);
		if (newVal != null)
			viewFCD2_Low.setText(newVal.getStringVal());
		
		newVal = checkFieldChange(kFldInd_FCD2_High, FldInfo.FldIndices.Weath_FCD2_High, curFlds);
		if (newVal != null)
			viewFCD2_High.setText(newVal.getStringVal());
		
		
		// Day 3 forecast stuff
		newVal = checkFieldChange(kFldInd_FCD3_Stamp, FldInfo.FldIndices.Weath_FCD3_Stamp, curFlds);
		if (newVal != null)
			viewFCD3_Stamp.setText(GenHelpers.tmToFull(newVal.getStringVal()));
		
		newVal = checkFieldChange(kFldInd_FCD3_Text, FldInfo.FldIndices.Weath_FCD3_CondText, curFlds);
		if (newVal != null)
			viewFCD3_Text.setText(newVal.getStringVal());
		
		updateIcon(kFldInd_FCD3_Icon, FldInfo.FldIndices.Weath_FCD3_Icon, viewFCD3_Icon);
		
		newVal = checkFieldChange(kFldInd_FCD3_Low, FldInfo.FldIndices.Weath_FCD3_Low, curFlds);
		if (newVal != null)
			viewFCD3_Low.setText(newVal.getStringVal());
		
		newVal = checkFieldChange(kFldInd_FCD3_High, FldInfo.FldIndices.Weath_FCD3_High, curFlds);
		if (newVal != null)
			viewFCD3_High.setText(newVal.getStringVal());		
    }


	// -----------------------------------------------------------------------
    //	Data members
	// -----------------------------------------------------------------------
    
    // The last thread state we saw, so we know if it transitions
    private ThreadStates	lastThreadState;
    
    //
    //	We remember the last room serial number we got. We can check this
    //	periodically and see if it's changed. If so, we need to reconfigure
    //	ourself since the config info has changed.
    //
    //	We store the last weather moniker we got, and the room serial number
    //	active at that point. 
    //
    private int				lastRoomSerNum;
    
    //
    //	We have to track field info for the good number of fields associated 
    //	with current and forecast weather conditions.
    //
    //	We update these from the underlying base class when they have changed.
    //	We may get nulls if the field is not available.
    //
    //	We also have to remember the underlying indices of these fields
    //	in our base class' overall field list. So we use the extended
    //	field info object, that lets us store both.
    //
    private Vector<FldInfo>	curFlds;
    
    // We look up our views that we need to update
    TextView				viewCurBaro;
    TextView				viewCurCondText;
    TextView				viewCurFCText;
    TextView				viewCurHumidity;
    ImageView				viewCurIcon;
    TextView				viewCurTemp;
    TextView				viewCurWndSp;
    
    TextView				viewFCD1_High;
    ImageView				viewFCD1_Icon;
    TextView				viewFCD1_Low;
    TextView				viewFCD1_Stamp;
    TextView				viewFCD1_Text;
    
    TextView				viewFCD2_High;
    ImageView				viewFCD2_Icon;
    TextView				viewFCD2_Low;
    TextView				viewFCD2_Stamp;
    TextView				viewFCD2_Text;
    
    TextView				viewFCD3_High;
    ImageView				viewFCD3_Icon;
    TextView				viewFCD3_Low;
    TextView				viewFCD3_Stamp;
    TextView				viewFCD3_Text;    
}
