//
//	This is the home activity, which is the first one displayed. It provides a
//	login screen to let the user enter user name, password, and target server
//	info, and optionally to store it for subsequent use, or to select from
//	an already stored configuration.
//
//	We display the connection status info. Once the connection succeeds, we
//	move on to the main activity screen.
//
package com.charmedquark.cqcandroidc;

import java.util.Vector;

import android.os.Bundle;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.content.Intent;

import com.charmedquark.xmlgwclient.GenHelpers;

// We need to invoke the home activity
import com.charmedquark.cqcandroidc.HomeActivity;

public class LoginActivity extends GWActivity
{
	public LoginActivity()
	{
		super();
		
		//
		// 	Initialize the last connection state we saw. We start with the
		//	worst case and that's what our text widget initial is set up to,
		//	so we'll be in sync until we see a change.
		//
		lastState = ThreadStates.WaitConfig;
		roomList = new String[1];
		roomList[0] = new String();
		roomCnt = 0;
	}

    // -----------------------------------------------------------------------
	//	Methods we have to override from our common activity base class
    // -----------------------------------------------------------------------
	@Override
	protected int getLayoutResourceId()
	{
		return R.layout.activity_login;
	}
	
	
    // -----------------------------------------------------------------------
	//	Lifecycle methods we have to handle
    // -----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
		// Wire up the handler for our login button 
		((Button)findViewById(R.id.LoginButton)).setOnClickListener(loginHandler);

		// And the item click handler for the room list
		((ListView)findViewById(R.id.RoomList)).setOnItemClickListener(roomClickHandler);
		
		// On this one, we don't want to show the home button
		findViewById(R.id.HomeButton).setVisibility(View.INVISIBLE);
		
		// If we have stored settings, load them up for editing/acceptance
		SharedPreferences settings = getSharedPreferences(kPrefs_LoginPrefs, 0);
		String curVal;
		int curPort;
		boolean loadPW;
		
		curPort = settings.getInt(kPrefs_SrvPort, kDefPort);
		{
			// Format the port out to text and load it up
			Integer portFmt = Integer.valueOf(curPort);
			curVal = portFmt.toString();
			((TextView)findViewById(R.id.SrvPort)).setText(curVal);
		}

		curVal = settings.getString(kPrefs_SrvAddr, null);
		if (curVal != null)
			((TextView)findViewById(R.id.SrvAddress)).setText(curVal);			
		
		curVal = settings.getString(kPrefs_User, null);
		if (curVal != null)
			((TextView)findViewById(R.id.UserName)).setText(curVal);

		loadPW = settings.getBoolean(kPrefs_SavePW, false);
		if (loadPW)
			((CheckBox)findViewById(R.id.savePW)).setChecked(loadPW);

		curVal = settings.getString(kPrefs_Password, null);
		if (curVal != null)
			((TextView)findViewById(R.id.Password)).setText(curVal);
		
        // Enable our update timer
        enableUpdateTimer(updateTimer);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    }

    @Override
    protected void onStop()
    {
    	super.onStop();
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();

    	//
    	// 	Update visibility/content of some stuff based on current state.
    	//	In this case, we don't see if the state is different, we just
    	//	update and store the new last state.
    	//
    	lastState = getState();
    	setForState(lastState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.activity_home, menu);
        return true;
    }

    
    // -----------------------------------------------------------------------
    // 	An update timer we register with our parent class. It'll get
    //	called periodically so we can update status info.
    // -----------------------------------------------------------------------    
    private Runnable updateTimer = new Runnable()
    {
    	public void run()
    	{
    		// Update the connection status
    		ThreadStates curState = getState();
    		if (curState != lastState)
    		{
    			lastState = curState;

    			// Do things in response to the change
    			switch(curState)
    			{
    				case WaitConfig :
    					break;

    				case WaitConnect :
    					break;

    				case WaitRoom :
    					//
    					//	We have connected, So query the list of rooms
    					//	and load them into the room list.
    					//
    					loadRooms();
    					break;
    					
    				case RoomInit :
    					break;
    					
    				case Ready :
    				{
    					// Kick off the home activity now that we are ready
        				Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        				startActivity(intent);
    					break;
    				}
    					
    				case Disconn :
    					break;
    			};
    			
    			//
    			// 	Update text and visibility stuff based on our new current
    			//	state.
    			//
    			setForState(curState);
    		}   
    	}
    };

    // -----------------------------------------------------------------------
    // Handlers for user interaction
    // -----------------------------------------------------------------------
    
    // The rock selection list click handler
    private OnItemClickListener roomClickHandler = new OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> adapter, View tarView, int position, long id)
        {
        	// If we aren't in WaitRoom state or beyond, then we don't do anything
        	if (lastState.ordinal() >= ThreadStates.WaitRoom.ordinal())
        	{
        		// 	Ask the base class to set this new room
        		String roomName = (String)adapter.getAdapter().getItem(position);
        		setRoom(roomName);
        	}
        }
    };
    
    // A handler for our login button 	
    private Button.OnClickListener loginHandler = new Button.OnClickListener()
    {  
		public void onClick(View v)
		{
			//
			//	If we are connected or trying to connect, then this should do
			//	a disconnect. Else, it does a connect.
			//
			if (lastState.ordinal() >= ThreadStates.WaitConnect.ordinal())
			{
				disconnect();
			}
			 else
			{
				String password = null;
				String srvAddr = null;
				String srvPort = null;
				String userName = null;
				boolean savePW = false;
				
				// Get the values out of the views and try a login
				TextView pwText = (TextView)findViewById(R.id.Password);
				
				srvAddr = ((TextView)findViewById(R.id.SrvAddress)).getText().toString();
				srvPort = ((TextView)findViewById(R.id.SrvPort)).getText().toString();
				userName = ((TextView)findViewById(R.id.UserName)).getText().toString();
				savePW = ((CheckBox)findViewById(R.id.savePW)).isChecked();
				password = pwText.getText().toString();
				
				// The port has to be a valid number and none of them can be empty
				if (password.isEmpty() || srvAddr.isEmpty()
				||  srvPort.isEmpty() || userName.isEmpty())
				{
					return;
				}
				
				int portNum = GenHelpers.textToInt(srvPort, kDefPort);
	
				//
				// 	OK, let's set the info on our parent class to get him to connect.
				//	If he's connected, this will force a disconnect, so it'll then
				//	start reconnecting with this new info.
				//
				setConnInfo(userName, password, srvAddr, portNum);

				// If not asked to save the PW, force it to an empty string
				if (!savePW)
					password = "";
				
				// Store this info for the next time
				SharedPreferences settings = getSharedPreferences(kPrefs_LoginPrefs, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(kPrefs_SavePW, savePW);
				editor.putInt(kPrefs_SrvPort, portNum);
				editor.putString(kPrefs_SrvAddr, srvAddr);
				editor.putString(kPrefs_User, userName);
				editor.putString(kPrefs_Password, password);
				editor.commit();
			}
		}
    };

    
    // -----------------------------------------------------------------------
    //	Private helper methods
    // -----------------------------------------------------------------------
    private void loadRooms()
    {
    	Vector<String> list = null;
    	try
    	{
    		list = new Vector<String>();
    		getRoomList(list);
    	}
    	
        catch(RuntimeException e)
        {
        	return;
        }    		
    		
		roomCnt = list.size();
		roomList = new String[roomCnt];
		for (int index = 0; index < roomCnt; index++)
			roomList[index] = list.get(index);
		
		ListView listView = (ListView)findViewById(R.id.RoomList);
		ArrayAdapter<String> roomAdapter = new ArrayAdapter<String>
		(
			this
			, R.layout.listlayout_med_text
			, roomList
		);
		listView.setAdapter(roomAdapter);
    }
    
    //
    // 	We hide or show some stuff, and set some text based on state. This
    //	helper does that work, since we have to do it both while watching
    //	for thread state changes, and when we are resumed.
    //
    //	We assume this is only called if the state has changed.
    //
    private boolean setForState(ThreadStates forState)
    {
    	boolean roomListVis = false;
		int stateMsgId = 0;
		int connButtMsgId = R.string.butt_login;
		TextView stateView = (TextView)findViewById(R.id.ConnStatus);
		TextView connButtView = (TextView)findViewById(R.id.LoginButton);
		switch(forState)
		{
			case WaitConfig :
				stateMsgId = R.string.connstat_waitconfig;
				break;

			case WaitConnect :
				connButtMsgId = R.string.butt_cancel; 
				stateMsgId = R.string.connstat_waitconn;
				break;

			case WaitRoom :
				connButtMsgId = R.string.butt_disconn;
				stateMsgId = R.string.connstat_waitroom;
				roomListVis = true;
				break;
				
			case RoomInit :
				connButtMsgId = R.string.butt_disconn;
				stateMsgId = R.string.connstat_waitinit;
				roomListVis = true;
				break;
				
			case Ready :
				connButtMsgId = R.string.butt_disconn;
				stateMsgId = R.string.connstat_ready;
				roomListVis = true;
				break;
				
			case Disconn :
				stateMsgId = R.string.connstat_disconn;
				break;
		};

		if (connButtMsgId != 0)
			connButtView.setText(getString(connButtMsgId));
		
		if (stateMsgId != 0)
			stateView.setText(getString(stateMsgId));
		else
			stateView.setText("Unknown connection status");

		// 	If the room list is visible, and it's empty, load it up
		if (roomListVis)
		{
			ListView listView = (ListView)findViewById(R.id.RoomList);
			if (listView.getChildCount() == 0)
			{
				// If we've not gotten the list from the client, do that
				if (roomCnt == 0)
					loadRooms();

				ArrayAdapter<String> roomAdapter = new ArrayAdapter<String>
				(
					this
					, R.layout.listlayout_med_text
					, R.id.text1
					, roomList
				);
				listView.setAdapter(roomAdapter);
			}
		}
		
		// Hide or show the room list as indicated
		if (roomListVis)
			findViewById(R.id.RoomListGroup).setVisibility(View.VISIBLE);
		else
			findViewById(R.id.RoomListGroup).setVisibility(View.INVISIBLE);
		
		return true;
    }
    

    // The list of rooms we query from the parent class 
    private int roomCnt;
    private String[] roomList;
   
    // The last connection status we saw in our update timer
    private ThreadStates lastState;
    
    // The names we use for configuration storage
    private final static String kPrefs_LoginPrefs = new String("LoginPrefs");
    private final static String kPrefs_Password = new String("Password");
    private final static String kPrefs_SavePW = new String("SavePW");
    private final static String kPrefs_SrvAddr = new String("SrvAddr");
    private final static String kPrefs_SrvPort = new String("SrvPort");
    private final static String kPrefs_User = new String("UserName");
}
