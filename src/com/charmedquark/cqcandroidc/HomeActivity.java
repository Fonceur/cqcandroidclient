//
//	This is the home activity, which is the first one displayed. It provides a
//	login screen to let the user enter user name, password, and target server
//	info, and optionally to store it for subsequent use, or to select from
//	an already stored configuration.
//
//	We display the connection status info. Once the connection succeeds, we
//	move on to the main activity screen.
//
package com.charmedquark.cqcandroidc;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

// We need to import the functional areas activities
import com.charmedquark.cqcandroidc.LightingActivity;
import com.charmedquark.cqcandroidc.WeatherActivity;

public class HomeActivity extends GWActivity
{
	public HomeActivity()
	{
		super();
	}

	
	// -----------------------------------------------------------------------
	//	Method we have to override from our common activity base class
    // -----------------------------------------------------------------------
	@Override
	protected int getLayoutResourceId()
	{
		return R.layout.activity_home;
	}
	
	
    // -----------------------------------------------------------------------
	//	Lifecycle methods we have to handle
    // -----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

		// Wire up the handlers for our buttons 
		((Button)findViewById(R.id.LightingButton)).setOnClickListener(buttHandler);        
		((Button)findViewById(R.id.MediaButton)).setOnClickListener(buttHandler);
		((Button)findViewById(R.id.WeatherButton)).setOnClickListener(buttHandler);
		
        // Enable our update timer
        enableUpdateTimer(updateTimer);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    }

    @Override
    protected void onStop()
    {
    
    	super.onStop();
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.activity_home, menu);
        return true;
    }

    // -----------------------------------------------------------------------
    // 	An update timer we register with our parent class. It'll get
    //	called periodically so we can update status info.
    // -----------------------------------------------------------------------    
    private Runnable updateTimer = new Runnable()
    {
    	public void run()
    	{
    	}
    };

    
    // -----------------------------------------------------------------------
    //	User interaction handlers
    // -----------------------------------------------------------------------
    private Button.OnClickListener buttHandler = new Button.OnClickListener()
    {  
		public void onClick(View v)
		{
			// Depending on the button clicked, load the appropriate activity
			Intent intent = null;
			if (v.getId() == R.id.LightingButton)
				intent = new Intent(getApplicationContext(), LightingActivity.class);
			else if (v.getId() == R.id.MediaButton)
				intent = new Intent(getApplicationContext(), MBrowseActivity.class);			
			else if (v.getId() == R.id.WeatherButton)
				intent = new Intent(getApplicationContext(), WeatherActivity.class);
			
			if (intent != null)
				startActivity(intent);
        }
    };
}
