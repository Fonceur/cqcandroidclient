package com.charmedquark.cqcandroidc;


import java.io.ByteArrayInputStream;
import java.util.Vector;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.charmedquark.cqcandroidc.GWActivity.GWCmdRes;
import com.charmedquark.xmlgwclient.*;


public class MoviePreviewActivity extends GWActivity
{
	// -----------------------------------------------------------------------
	//	Constructor
	// -----------------------------------------------------------------------
	public MoviePreviewActivity()
	{
	}


	// -----------------------------------------------------------------------
	//	Methods we have to override from our common activity base class
    // -----------------------------------------------------------------------
	@Override
	protected int getLayoutResourceId()
	{
		return R.layout.activity_moviepreview;
	}


	// -----------------------------------------------------------------------
	//	Lifecycle methods we have to handle
	// -----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        // Get out the incoming info from the intent
        Intent ourIntent = getIntent();
    	colCookie = ourIntent.getStringExtra("ColCookie");
    	dirName = ourIntent.getStringExtra("Director");
    	repoMoniker = ourIntent.getStringExtra("Repo");
    	titleName = ourIntent.getStringExtra("TitleName");
    	relYear = ourIntent.getStringExtra("Year");
    	
    	// Load up the info into our display views
    	yearView = (TextView)findViewById(R.id.contYear);
    	yearView.setText(relYear);
    	
    	titleView = (TextView)findViewById(R.id.titleText);
    	titleView.setText(titleName);
    	
    	//
    	// 	Start up the download of the cover art. It will
    	//	be displayed when it completes.
    	//
    	coverArt = (ImageView)findViewById(R.id.coverArt);
    	new LoadCoverArt().execute();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    }

    @Override
    protected void onStop()
    {
       	super.onStop();
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }

   
	// -----------------------------------------------------------------------
    //	Handlers for user interaction
	// -----------------------------------------------------------------------
    
    // A handler for our login button 	
    private Button.OnClickListener buttonHandler = new Button.OnClickListener()
    {  
		public void onClick(View v)
		{
			// 
		}
	};

	// -----------------------------------------------------------------------
    //	Private helper methods
	// -----------------------------------------------------------------------

	private class LoadCoverArt extends AsyncTask<Void, Integer, Boolean>
    {
		Bitmap bmpLoad = null;
		Exception failErr = null;
		
		protected Boolean doInBackground(Void... parms)
    	{
			try
			{
	   			String msgOut = GWClient.buildSimpleMsg
				(
					"CQCGW:QueryMediaImg"
					, "CQCGW:Moniker", repoMoniker
					, "CQCGW:Type", "Col"
					, "CQCGW:Size", "Sml"
					, "CQCGW:Cookie", colCookie
				);
	
				// Queue up the query and get the result
				GWCmdRes qRes = sendQuery(msgOut, "CQCGW:MediaImgData");
				if (!qRes.cmdStatus)
					throw(qRes.err);

				//
				// 	The image data is Base64 encoded in the body text
				//	of the reply. Find the text child of the msg element.
				//
				final String resText = GWClient.getMsgTextContent(qRes.resMsg);				
				byte[] imgData = Base64.decode(resText, Base64.DEFAULT);
				bmpLoad = BitmapFactory.decodeByteArray
				(
					imgData,  0,  imgData.length
				);
			}
			
			catch(Exception e)
			{
				failErr = e;
				return false;
			}
			return true;
    	}

    	protected void onPostExecute(Boolean res)
    	{
    		if (res)
    		{
    			// We got the image, so load it to the view
    			try
    			{
    				coverArt.setImageBitmap(bmpLoad);
    			}
    			
    			catch(Exception e)
    			{
   	   			 	showMsg("Couldn't display cover art", e.getMessage());    				
    			}
    		}
	   		 else
	   		{
	   			 showMsg("Failed to get image data", failErr.getMessage());	
	   		}    		
    	}
	}	
	
	
	// -----------------------------------------------------------------------
    //	Data members
	// -----------------------------------------------------------------------
    
	//
	// 	To store the info that comes to us in the intent that starts us. The
	//	browser activity retrieves the info before bothering to call us. to
	//	make sure it's obtainable. Or OnCreate pulls the values out and stores
	//	them for our later use.
	//
	private String colCookie;
	private String dirName;
	private String repoMoniker;
	private String titleName;
	private String relYear;
	
	// The views that display our info
	ImageView		coverArt;
	TextView		titleView;
	TextView		yearView;
}
