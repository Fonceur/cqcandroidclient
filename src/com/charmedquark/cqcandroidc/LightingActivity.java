package com.charmedquark.cqcandroidc;

import java.util.Vector;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.charmedquark.xmlgwclient.*;
import com.charmedquark.xmlgwclient.LoadInfo.LoadTypes;

public class LightingActivity extends GWActivity
{
	// -----------------------------------------------------------------------
	//	Some class constants
	// -----------------------------------------------------------------------	
	final int kMaxLoads = 4;
	
	public LightingActivity()
	{
		super();

		//
		// 	Init our data. We have to create vectors for our loads and their
		//	associated fields.
		//
	    curLoads = new Vector<LoadInfo>();
	    curFlds = new Vector<FldInfo>();
	}
	
	
	// -----------------------------------------------------------------------
	//	Methods we have to override from our common activity base class
    // -----------------------------------------------------------------------
	@Override
	protected int getLayoutResourceId()
	{
		return R.layout.activity_lighting;
	}


	// -----------------------------------------------------------------------
	//	Lifecycle methods we have to handle
	// -----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        switchList = new CheckBox[kMaxLoads];
        switchList[0] = (CheckBox)findViewById(R.id.loadSwitch1);
        switchList[1] = (CheckBox)findViewById(R.id.loadSwitch2);
        switchList[2] = (CheckBox)findViewById(R.id.loadSwitch3);
        switchList[3] = (CheckBox)findViewById(R.id.loadSwitch4);
        for (int index = 0; index < kMaxLoads; index++)
        	switchList[index].setOnCheckedChangeListener(checkHandler);

        textList = new TextView[kMaxLoads];
        textList[0] = (TextView)findViewById(R.id.loadName1);
        textList[1] = (TextView)findViewById(R.id.loadName2);
        textList[2] = (TextView)findViewById(R.id.loadName3);
        textList[3] = (TextView)findViewById(R.id.loadName4);
        
        dimList = new SeekBar[kMaxLoads];
        dimList[0] = (SeekBar)findViewById(R.id.loadDim1);
        dimList[1] = (SeekBar)findViewById(R.id.loadDim2);
        dimList[2] = (SeekBar)findViewById(R.id.loadDim3);
        dimList[3] = (SeekBar)findViewById(R.id.loadDim4);
        for (int index = 0; index < kMaxLoads; index++)
        	dimList[index].setOnSeekBarChangeListener(dimHandler);

        // Make sure we get an initial thread state transition
        lastThreadState = null;

        // And make sure we get new room config
	    lastRoomSerNum = 0;

        // Enable our update timer
        enableUpdateTimer(updateTimer);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();

    	//	Do an initial refresh to make sure we are in sync.
    	refreshView();
    }

    @Override
    protected void onStop()
    {
       	super.onStop();
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }

	// -----------------------------------------------------------------------
    // 	An update timer we register with our parent class. It'll get
    //	called periodically so we can update status info.
	// -----------------------------------------------------------------------
    private Runnable updateTimer = new Runnable()
    {
    	public void run()
    	{
    		// Do any updating required
    		refreshView();
    	}
    };

    
	// -----------------------------------------------------------------------
    //	Private helper methods
	// -----------------------------------------------------------------------
    
    //
    //	This is called upon creation, if our timer sees that the room
    //	config has changed, or when we are started up again. We see if the
    //	info we have is still valid. If not, we update ourself. WE have to
    //	get the new room list, init our field list, and update our display
    //	appropriately to reflect the loads we have.
    //
    private void refreshView()
    {
    	//
    	//	Get the current state of the bgn thread, and the latest room
    	//	config serial number.
    	//
    	final ThreadStates curState = getState();
    	final int curSerNum = getRoomSerialNum();

    	// If either the thread state or the serial number has changed, see what's up
    	if ((lastThreadState == null)
    	||  (lastThreadState != curState)
    	||  (curSerNum != lastRoomSerNum))
    	{
    		// We can update the thread state either way
    		lastThreadState = curState;
    		
    		//
    		//	If we either are not ready, or we have no room config, we have to
    		//	just disable everything and do nothing.
    		//
    		if ((curState != ThreadStates.Ready) || (curSerNum == -1))
    		{
    			//
	    		// 	At this point, we aren't going to get to the code below,
    			//	so update the last serial number now. Even if it's -1, that's
    			//	fine, that insures we see a change if it goes to anything
    			//	that indicates we have content.
    			//
    			//	We don't want to store it otherwise, since we want to see
    			//	below 
    			//
    			lastRoomSerNum = curSerNum;
	   		
	    		int index = 0;
	    		while (index < kMaxLoads)
	    		{
	    			textList[index].setVisibility(View.VISIBLE);
	    			switchList[index].setVisibility(View.VISIBLE);
	    			dimList[index].setVisibility(View.VISIBLE);
	    			
	    			textList[index].setEnabled(false);
	    			switchList[index].setEnabled(false);
	    			dimList[index].setEnabled(false);
	    			index++;
	    		}
	    		return;
	    	}
    	}
    	
    	//
    	// 	See if our room configuration has changed. The code above handled
    	//	the situation where it was -1 (meaning none available, so we know
    	//	we have some data here.
    	//
    	if (getRoomSerialNum() != lastRoomSerNum)
    	{
    		// Get a new load list and the latest serial number
    		lastRoomSerNum = getLoadList(curLoads);

    		//
    		// 	Flush our field list and then loop through the loads we got 
    		//	and load up our field list with nulls. We do this, instead of
    		//	getting the initial field info objects here, so that the code
    		//	below will be forced to get new fields and see them as
    		//	changed and get our visual elements initialized.
    		//
    		//	We store in the extra indices of the load objects the indices
    		//	of each load's fields in our own local field list. We do this
    		//	just by keeping a count of items added so far.
    		//
    		final int loadCnt = curLoads.size();
    		int fldCnt = 0;
    		curFlds.removeAllElements();
    		for (int index = 0; index < loadCnt; index++)
    		{
    			LoadInfo curLoad = curLoads.get(index);
    			switch(curLoad.loadType)
    			{
    				case Both :
    					// We have two fields
    					curLoad.ex1Index = fldCnt++;
    					curLoad.ex2Index = fldCnt++;
    					curFlds.add(null);
    					curFlds.add(null);
    					break;

    				default :
    					curLoad.ex1Index = fldCnt++;
    					curFlds.add(null);
    					break;
    			};
    		}

    		//
        	// 	Based on the count of loads, we hide/show stuff and set text.
    		//	First make a pass for the ones that are defined. Set the name
    		//	of the loads on the text displays, enable the controls as
    		//	appropriate for the type of loads, and make them visible.
    		//
    		int index = 0;
    		while (index < loadCnt)
    		{
    			LoadInfo curLoad = curLoads.get(index);
    			textList[index].setText(curLoad.loadName);
    			
    			textList[index].setEnabled(true);
    			
    			if (curLoad.loadType == LoadTypes.Dimmer)
    			{
    				dimList[index].setEnabled(true);
    				switchList[index].setEnabled(false);
    			}
    			 else if (curLoad.loadType == LoadTypes.Switch)
    			{
    				 switchList[index].setEnabled(true);
    				 dimList[index].setEnabled(false);    				
    			}
    			 else
   			    {
    				 dimList[index].setEnabled(true);
    				 switchList[index].setEnabled(true);
   			    }
    			
    			// Set the min/max values on the slider if appropriate
    			if ((curLoad.loadType == LoadTypes.Dimmer)
    			||  (curLoad.loadType == LoadTypes.Both))
    			{
    				dimList[index].setMax(curLoad.dimMax);
    			}
    			
    			textList[index].setVisibility(View.VISIBLE);
    			switchList[index].setVisibility(View.VISIBLE);
    			dimList[index].setVisibility(View.VISIBLE);
    			
    			index++;
    		}

    		// Hide any remaining
    		while (index < kMaxLoads)
    		{
    			textList[index].setVisibility(View.INVISIBLE);
    			switchList[index].setVisibility(View.INVISIBLE);
    			dimList[index].setVisibility(View.INVISIBLE);
    			index++;
    		}
    	}

    	// See if any of our fields have changed and update if so
		final int loadCnt = curLoads.size();
		for (int index = 0; index < loadCnt; index++)
		{
			LoadInfo curLoad = curLoads.get(index);
			
			// If we don't have field info yet, nothing to do
			if (curLoad.ex1Index == -1)
				continue;

			//
			// 	We might not have the fields in our list yet. If not, we have to
			//	pass in zero for the serial number, which makes sure we get one.
			//  Else we pass in the last serial number we had for that field.
			//
			FldInfo newVal1 = checkFieldChange(curLoad.ex1Index, curLoad.fld1Index, curFlds);
			FldInfo newVal2 = null;
			if (curLoad.loadType == LoadTypes.Both)
				newVal2 = checkFieldChange(curLoad.ex2Index, curLoad.fld2Index, curFlds);

			// If either changed, then update
			if ((newVal1 != null) || (newVal2 != null))
			{
				switch(curLoad.loadType)
				{
					case Both :
						// Dimmer is first value and switch the second
						if (newVal1 != null)
						{
							dimList[index].setProgress(newVal1.getIntVal(curLoad.dimMin));
							curFlds.set(curLoad.ex1Index, newVal1);
						}
						if (newVal2 != null)
						{
							curFlds.set(curLoad.ex2Index, newVal2);
							switchList[index].setChecked(newVal2.getBoolVal());
						}
						break;
	
					case Dimmer :
						if (newVal1 != null)
						{
							curFlds.set(curLoad.ex1Index, newVal1);
							dimList[index].setProgress(newVal1.getIntVal(curLoad.dimMin));
						}
						break;
	
					case Switch :
						if (newVal1 != null)
						{
							curFlds.set(curLoad.ex1Index, newVal1);
							switchList[index].setChecked(newVal1.getBoolVal());
						}
						break;
						
					default :
						break;
				};
			}
		}
	}

    
    // -----------------------------------------------------------------------
    //	User interaction handlers
    // -----------------------------------------------------------------------
    private CompoundButton.OnCheckedChangeListener checkHandler = new CompoundButton.OnCheckedChangeListener()
    {  
		public void onCheckedChanged(CompoundButton butt, boolean newState)
		{
			int loadInd;
			
			// Depending on the button clicked, load the appropriate activity
			if (butt.getId() == R.id.loadSwitch1)
				loadInd = 0;
			else if (butt.getId() == R.id.loadSwitch2)
				loadInd = 1;			
			else if (butt.getId() == R.id.loadSwitch3)
				loadInd = 2;
			else if (butt.getId() == R.id.loadSwitch4)
				loadInd = 3;
			else
				return;

			LoadInfo tarLoad = curLoads.get(loadInd);
			
			// Figure out which field to write to
			String tarFld;
			if (tarLoad.loadType == LoadTypes.Both)
				tarFld = tarLoad.fld2Name;
			else
				tarFld = tarLoad.fld1Name;
			
			writeField(tarFld, newState ? "True" : "False", 0);
        }
    };

    private SeekBar.OnSeekBarChangeListener dimHandler = new SeekBar.OnSeekBarChangeListener()
    {
    	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    	{
    	}
    	
    	public void onStartTrackingTouch(SeekBar seekBar)
    	{
    	}
    	
    	public void onStopTrackingTouch(SeekBar seekBar)
    	{
    		// Figure out which seek bar
			int loadInd;
			
			// Depending on the button clicked, load the appropriate activity
			if (seekBar.getId() == R.id.loadDim1)
				loadInd = 0;
			else if (seekBar.getId() == R.id.loadDim2)
				loadInd = 1;			
			else if (seekBar.getId() == R.id.loadDim3)
				loadInd = 2;
			else if (seekBar.getId() == R.id.loadDim4)
				loadInd = 3;
			else
				return;

    		// Set the current progress on the dimmer level of this load
			LoadInfo tarLoad = curLoads.get(loadInd);
			
			String newLevel;
			newLevel = String.format("%d", seekBar.getProgress());
			writeField(tarLoad.fld1Name, newLevel, 0);
			
			//
			//	It worked, so update our local field with this new value. This
			//	avoids the issue where the bar goes back to its current value
			//	until the new field value is seen.
			//
//			curFlds.get(curLoads.get(loadInd).ex1Index).setValue(newLevel);
    	}
    };

    
	// -----------------------------------------------------------------------
    //	Data members
	// -----------------------------------------------------------------------
    
    // The last thread state we saw, so we know if it transitions
    private ThreadStates		lastThreadState;
    
    //
    //	We remember the last room serial number we got. We can check this
    //	periodically and see if it's changed. If so, we need to reconfigure
    //	ourself since the config info has changed.
    //
    //	And we have the list of lighting loads we are to display. We get the
    //	serial number when we query it.
    //
    private int					lastRoomSerNum;
    private Vector<LoadInfo>	curLoads;
    
    //
    //	We have to track field info for the fields associated with our loads.
    //	We update these from the underlying base class when they have changed.
    //	We may get nulls if the field is not available.
    //
    private Vector<FldInfo>		curFlds;
    
    //
    //	The list of check boxes used for switches, the sliders used for
    //	dimmers, and the text for the load name.
    //
    private CheckBox			switchList[];
    private SeekBar				dimList[];
    private TextView			textList[];
}
