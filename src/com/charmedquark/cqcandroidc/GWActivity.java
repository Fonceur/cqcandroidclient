//
//	We use a common base activity class, so that we can manage a static
//	bgn thread that does the communications with the GW server. The derived
//	activity classes can provide us with a runnable target that we will
//	call periodically (on their thread) so that they can keep their display
//	info up to date.
//
//	So those two things disconnect the foreground and background. Access to
//	the state info between the timer driven fgn updates and the bgn thread 
//	synchronized by a mutex.
//
//	Our lifecycle is:
//
//	1. 	Ctor - If our lock is null, then we are just loaded or being reloaded
//		after being fully unloaded. We initialize static members and lock. We
//		set an initial thread state of waiting for config, since we don't have
//		any login info at this point.
//	2. 	OnCreate - Nothing. Derived classes can set up their own data as needed
//	3.  OnDestroy - Same as OnCreate. Derived classes can clean up. We don't
//		need to do anything.
//	4.	OnStart - Bump our activity reference count. If it goes to 1 from zero
//		we start up the bgn thread. We leave the bgn thread state whereever it
//		was (either wait for config or waiting for connect, depending on whether
//		we had gotten connection info before now or not.) If the ref count was
//		already non-zero, we do nothing. If there was a bgn thread stop time
//		stamp set (see onStop) we clear it again.
//	5.	OnStop - If the ref count is zero, we do nothing. If not, we decrement
//		the activity ref count. If it goes zero, we set a time stamp. The bgn
//		thread will stop itself at that time if we don't do anything to stop it.
//	6.	OnPause - We stop the fgn timer from being invoked, so the activity
//		stops updating the screen.
//	7.	OnResume - We re-enable the fgn timer again.
//
//
//	So, basically we have a bgn thread that will run as long as we have any
//	foreground activities, or have had one in the foreground within a period
//	of time. So if the user goes off and comes back we don't have to start 
//	over again. The thread will still be there and connected. If we are fully
//	unloaded or loading initially, then our lock object will be null and our
//	ctor will fault in the statics and our initial activity will load to get
//	login info from the user.
//
//	If the bgn thread shuts down, it leaves any previous connect info in place
//	and (if there is any) puts the state back to waiting to connect, so that
//	if the thread is restarted it will automatically reconnect. If we get fully
//	whacked the thread will have been stopped as well, and all the info will
//	get re-initialized again. If the thread never got connected, it will still
//	be waiting for config info.
//
//	The lock object we create is used to sync access to state info between
//	the timer driven activities (the timer runs in the GUI thread), and the
//	bgn thread.
//
package com.charmedquark.cqcandroidc;

import java.util.Vector;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.w3c.dom.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.charmedquark.xmlgwclient.*;

public abstract class GWActivity extends Activity
{
	// Our state machine enum
	public enum ThreadStates
	{
		WaitConfig, WaitConnect, WaitRoom, RoomInit, Ready, Disconn
	};
	
	// An enum for the overall status of each major functional chunk
	public enum CtrlOpt { NotUsed, Display, Control };	
	
	// The default port
	public final static int kDefPort = 13509;
	
	//
	//	We provide methods for the activities to call in order to get us to
	//	do some operations. But, internally we have to queue them up for the
	//	bgn thread to process. The methods generally block on the object that
	//	is queued up, waiting for the bgn thread to post it as completed.
	//
	public enum GWCmds
	{
		Disconnect, SendCmd, SendQuery
	}
	
	public static class GWCmdRes
	{
		GWCmdRes(Exception e)
		{
			cmdStatus = false;
			err = e;
			resMsg = null;
		}
		
		GWCmdRes(Element msgElem)
		{
			cmdStatus = true;
			err = null;
			resMsg = msgElem;
		}
		
		boolean cmdStatus;
		Exception err;
		Element resMsg;
	}
	
	public static class GWCmdInfo
	{
		GWCmdInfo(GWCmds toDo, String text, String text2)
		{
			cmdToDo = toDo;
			cmdText = text;
			cmdText2 = text2;
			cmdRes = null;
		}

		public GWCmds cmdToDo;
		public String cmdText;
		public String cmdText2;
		public GWCmdRes cmdRes;
	}
	
	
	
    // -----------------------------------------------------------------------
    //	Constructor and initialization methods 
    // -----------------------------------------------------------------------
	public GWActivity()
	{
		super();

		//
		// 	Use the lock sync as a fault in mechanism to fault in static stuff
		//	the first time any activity is created. We use the lock itself to
		//	know if we've been completely unloaded and our statics destroyed.
		//	If so, we have to create them again.
		//
		//	There is a small possibility of a race condition here, but the
		//	way apps work in Android, if this is the first activity of our
		//	app being loaded (or reloaded after the app being unloaded), then
		//	another one isn'g going to start up until we start it.
		//
    	if (lockSync == null)
    	{
    		lockSync = new Object();
    		synchronized(lockSync)
    		{
	    		// Init the current thread states to the first state
	    		curState = ThreadStates.WaitConfig;
	    		
				// Allocate connection info
				hostAddr = new String();
				gwClient = new GWClient();
				gwThread = new GWThread();
				password = new String();
				userName = new String();

				// Other general stuff we need to setup
				roomList = new Vector<String>();
				cmdQ = new LinkedBlockingQueue<GWCmdInfo>();
				roomName = new String();
				roomSerNum = 1;
				
				//
				//	Just in case we didn't get all the calls, reset the count
				//	to zero, since we know that there can't be any activities
				//	around anymore. Our lock would have only gone null if our
				//	whole app was unloaded. Also clear the thread shutdown
				//	time stamp.
				//
				activityRef = 0;
				thrStopTime = 0;
				
				changedFlds = new Vector<FldPollStatus>();
				loadList = new Vector<LoadInfo>();
				fldList = new Vector<FldInfo>();
				fldIndexMap = new int[FldInfo.FldIndices.FldCount.ordinal()];
    		}
    	}
	}

	//
	//	On create/destroy we don't really do much at this level. The derived
	//	activity class can create/clean up anything it needs at its own level.
	//	Almost everything here is static, and initialized in our ctor in a
	//	synchronized fashion, and the bits that aren't are things that are
	//	only set up if the derived class causes that to happen, which would
	//	only be after creation.
	//
	//	We do need to init the few non-statics we have.
	//
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //
        // 	Set the content view on behalf of our derived classes, who tell us
        //	us what layout resource to use. This way, we can set up a handler
        //	for the buttons that are in the common header of all activities.
        //
		setContentView(getLayoutResourceId());
		((ImageButton)findViewById(R.id.HomeButton)).setOnClickListener(hdrButtonHandler);

		// Init our timer related stuff, initially disabled
        fgnTimer = null;
        fgnTimerTar = null;
        timerPaused = true;

        //
    	// 	Get our connect button in sync with the current thread state. We fake
        //	a call to our timer handler to make it update.
        //
		lastTimerThrState = ThreadStates.WaitConfig;
		updateTimer.run();
		
        //
        // 	Bump our ref count. If we end up with 1, then we clearly are
        //	starting the first activity, so see if we need to start the
    	//	bgn thread.
        //
		synchronized(lockSync)
		{
			activityRef++;
			if (activityRef == 1);
			{
				// Start the thread if not already running
				if ((gwThread == null) || !gwThread.isAlive())
				{
					gwThread = new GWThread();
					stopFlag = false;
					gwThread.start();
				}
			}

			//
			// 	Make sure the thread shutdown time stamp is reset. In
			//	some cases, like a change of orientation, we know the
			//	thread is still going to be here after we come back,
			//	so we do this whether the thread is there or not.
			//
			thrStopTime = 0;
		}
    }

    @Override
    protected void onDestroy()
    {
		// Stop our fgn update timer if it was set up
		if (fgnTimer != null)
			fgnTimer.cancel();

        //	
        // 	Decrement our activity ref count. If it goes zero, then set a shutdown
        //	time for the bgn thread to stop if we don't come back by then.
        //
		synchronized(lockSync)
		{
			if (activityRef > 0)
				activityRef--;
			if (activityRef == 0)
				thrStopTime = SystemClock.elapsedRealtime() + 30000;
		}
		
        super.onDestroy();
    }

    
    //
    //	Ref count foreground activities. If we don't have any anymore, then
    //	set a timer to stop the bgn thread if we don't come back to the fgn
    //	within that period of time. See the main class comments.
    //
    @Override
    protected void onStart()
    {
    	super.onStart();
    }
    
    @Override
    protected void onStop()
    {
    	super.onStop();
    }    
    
    
    //
    //	Pause/resume stops the update timer for this activity. So it stops
    //	trying to update the screen display when it's fully or partially
    //	obscured, or after being destroyed of course. See the main class
    //	comments above.
    //
    @Override
    protected void onPause()
    {
        super.onPause();
        
        // Set the paused flag to stop the update timer
        timerPaused = true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        
        // Clear the paused flag to let the update timer go again
        timerPaused = false;
    }

    
    // -----------------------------------------------------------------------
    //	The derived class can provide us with a target runnable object that
    //	we will call back on the UI thread regularly, so it can do updates.
    //  And we run one of our own as well.
    // -----------------------------------------------------------------------
    public void enableUpdateTimer(Runnable updateTar)
    {
    	// Save away the update timer target if any
    	fgnTimerTar = updateTar;
    	
    	timerPaused = false;
    	fgnTimer = new Timer();
        fgnTimer.schedule
    	(
    		new TimerTask()
    		{
    			@Override
    			public void run()
    			{
    				// If not paused, then call the timer method
    				if (!timerPaused)
    					fgnTimerMethod();
    			}
    		}
    		, 0
    		, 500
    	);
    }
    
    private void fgnTimerMethod()
	{
    	// Run the derived class' timer handler if they provided it
    	if (fgnTimerTar != null)
    		this.runOnUiThread(fgnTimerTar);
		
		// Do our own timer update pass
		this.runOnUiThread(updateTimer);
	}

    
    // -----------------------------------------------------------------------
    //	General helper methods
    // -----------------------------------------------------------------------    
    
    //
    // 	Set new connect info. If we are currently connected, we force a 
    //	disconnect, so the bgn thread will pick up the new stuff. If we
    //	are in anything before that, we are already waiting to get
    //	connected and will start trying to use this new info.
    //
    public static void
    setConnInfo(String user, String pass, String srv, int port)
    {
    	synchronized(lockSync)
    	{
    		hostPort = port;
    		hostAddr = srv;
    		password = pass;
    		userName = user;
    		
    		if (curState.ordinal() > ThreadStates.WaitConnect.ordinal())
    			curState = ThreadStates.Disconn;

    	}
    }

    //
    //	A bit of a hack to create a modal msg dialog. This one displays a title
    //	and message and waits for the user to hit a button.
    //
    public void showMsg(String title, String message)
    {
        
        final Handler handler = new Handler()
        {
            @Override
            public void handleMessage(Message mesg)
            {
                throw new RuntimeException();
            } 
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(true);
        alert.setNeutralButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                handler.sendMessage(handler.obtainMessage());
            }
        });
        alert.show();

        // Loop till a runtime exception is triggered.
        try { Looper.loop(); }
        catch(RuntimeException e) {}
    }    
    
    // -----------------------------------------------------------------------
    //	Methods for the foreground activity thread to query info or queue up
    //	commands.
    // -----------------------------------------------------------------------    

    //
    // 	Let's the derived activities ask for a disconnect. We just queue up
    //	a disconnect command on the background thread. If it turns out not to
    //	be meaningful, the bgn thread will just ignore it.
    //
    public void disconnect()
    {
    	// No need to sync, the queue is thread safe
    	cmdQ.add(new GWCmdInfo(GWCmds.Disconnect, "", ""));
    }
    
    //	
    // 	Let the foreground get a field's value. They pass us the index
    //	of the field to check, and the last field serial number they
    //	got. If the field's serial number is different, then we return
    //	then a copy of the field info object, which they can store away
    //	as their new value. It also includes the latest serial number,
    //	and good value/error state info.
    //
    public FldInfo checkFieldVal(FldInfo.FldIndices fldIndex, int curSerialNum)
    {
    	FldInfo retVal = null;
    	synchronized(lockSync)
    	{
    		// If we aren't connected, then no field info to return 
    		if (curState == ThreadStates.Ready)
    		{
    			//
    			// 	Map from the enum to the poll list index. If -1, not
    			//	available.
    			//
    			final int realInd = fldIndexMap[fldIndex.ordinal()];
    			if (realInd != -1)
    			{
    				FldInfo fldCheck = fldList.get(realInd);
    				if (fldCheck.valSerialNum != curSerialNum)
    					retVal = new FldInfo(fldCheck);
    			}
    		}
    	}
    	return retVal;
    }
    
    //
    //	And a slightly higher level helper that works in terms of the
    //	checkFieldVal method above. In this one, we handle a common
    //	operation of checking for a field change and updating a slot in a
    ///	derived class' list of field info objects.
    //
    //	They pass us the slot in their list and their list, and the index
    //	into our poll list. We'll check for a new value and update their
    //	list slot if so, and return either null if no change or the new
    //	object we put into the list.
    //
	public FldInfo checkFieldChange(int callerIndex
									, FldInfo.FldIndices pollIndex
									, Vector<FldInfo> callerList)
	{
		FldInfo curFld = callerList.get(callerIndex);
		int serNum = 0;
		if (curFld != null)
			serNum = curFld.valSerialNum; 
		FldInfo newVal = checkFieldVal(pollIndex, serNum);
		
		// If it changed, so update our field list at this slot
		if (newVal != null)
			callerList.set(callerIndex, newVal);
		return newVal;
	}


    //	
    // 	Return a list of configured loads. If we can provide the valid, we
    //	return the current room serial number, else -1.
    //
    public int getLoadList(Vector<LoadInfo> toFill)
    {
    	int retVal = -1;
    	
    	// Add copies of our loads to the caller's list
    	toFill.removeAllElements();
    	synchronized(lockSync)
    	{
    		// If we aren't ready, then we have no info
    		if (curState.ordinal() < ThreadStates.Ready.ordinal())
    			return retVal;

    		final int loadCnt = loadList.size();
    		for (int index = 0; index < loadCnt; index++)
    			toFill.add(new LoadInfo(loadList.get(index)));
    		
    		// Return the lastest room serial number
    		retVal = roomSerNum;
    	}
    	return retVal;
    }

    
    // Get the various monikers we deal with
    public String getMonMovieRend()
    {
    	return monMovieRend;
    }
    
    public String getMonMovieRepo()
    {
    	return monMovieRepo;
    }

    public String getMonMusicRend()
    {
    	return monMusicRend;
    }
    
    public String getMonMusicRepo()
    {
    	return monMusicRepo;
    }
    
    public String getMonWeather()
    {
    	return monWeather;
    }

    
    // Get the control option for the optional bits
    public CtrlOpt getOptMedia()
    {
    	return optMedia;
    }
    
    public CtrlOpt getOptSecurity()
    {
    	return optSecurity;
    }

    public CtrlOpt getOptWeather()
    {
    	return optWeather;
    }
    
    //
    //	Get the current list of rooms that the bgn thread has downloaded. If
    //	we aren't at least up to the WaitRoom state, then we know it can't
    //	be good. If we can't get the info, we return -1, else we return the
    //	current room list serial number.
    //
    public int getRoomList(Vector<String> toFill)
    {
    	int retVal = -1;
    	synchronized(lockSync)
    	{
    		if (curState.ordinal() < ThreadStates.WaitRoom.ordinal())
    			return -1;

    		toFill.removeAllElements();
    		final int roomCnt = roomList.size();
    		for (int index = 0; index < roomCnt; index++)
    			toFill.add(roomList.get(index));
    		
    		// Return the current room serial number
    		retVal = roomSerNum;
    	}    	
    	return retVal;
    }

    //
    //	Return the current room info serial number. This lets the derived
    //	activities know if they have out of data room config info. It's
    //	bumped every time we load the room list. Don't bother locking for
    //	just returning a simple value.
    //
    public int getRoomSerialNum()
    {
    	return roomSerNum;
    }

    
    //
    // 	Return the current thread state, so it knows what the state of the
    //	state is.
    //
    public ThreadStates getState()
    {
    	ThreadStates retVal;
    	synchronized(lockSync)
    	{
    		retVal = curState;
    	}
    	return retVal;
    }

    
    //
    //	The login screen calls this if the user selects a room, either
    //	initially, or comes back and changes rooms. We see if it's
    //	reasonable to do so. If so, we store the new name and update
    //	the thread state to make it start loading up this new room.
    //
    public boolean setRoom(String newRoom)
    {
     	synchronized(lockSync)
    	{
     	   	// We have to be at least up to wait room state
     		if (curState.ordinal() < ThreadStates.WaitRoom.ordinal())
    			return false;
    
    		// Store the new room name
    		roomName = newRoom;
    		
    		// Set the state to room initialization
    		curState = ThreadStates.RoomInit;
    		
    		//
    		// 	Bump the room serial number since this means that the
    		//	derived classes will now have out of date info.
    		//
    		roomSerNum++;
    	}
    	return true;
    }

    //
    //	We just queue up a command on behalf of the caller. If they ask, we
    //	will wait up to a particular time for the command to be processed,
    //	which means the bgn thread sent it and got the ack back.
    //
    public boolean sendCmd(String cmdMsg, int waitMSs)
    {
		GWCmdInfo cmdNew = null;
		cmdNew = new GWCmdInfo(GWCmds.SendCmd, cmdMsg, ""); 
		cmdQ.add(cmdNew);
    	
    	// Wait for it to complete if asked
    	if (waitMSs != 0)
		{
			try
			{
				synchronized (cmdNew)
				{
					cmdNew.wait(waitMSs);
				}
			}
			
			catch(InterruptedException e)
			{
				return false;
			}
		}
    	return true;
    }

    
    //
    //	We just do a send query command on behalf of the caller. We send the
    //	query, wait for a reply, and return the result.
    //
    public GWCmdRes sendQuery(String qMsg, String extRep)
    {
    	GWCmdInfo cmdRet = new GWCmdInfo(GWCmds.SendQuery, qMsg, extRep);
    	cmdQ.add(cmdRet);

    	try
    	{
    		// Wait for it to be processed
    		synchronized(cmdRet)
    		{
    			cmdRet.wait(4000);
    		}
    	}

    	catch(Exception e)
    	{
    		return new GWCmdRes(e);
    	}
    	return cmdRet.cmdRes;
    }

    
	// 
	//	Write a value to a field. One takes separate device/field names, 
	//	combines them, and calls the other version that takes them as one.
    //	The other one queues it up for processing by the bgn thread.
	//
	public boolean
	writeField(String devName, String fldName, String value, int waitMSs)
	{
		String tarFld = new String(devName + "." + fldName);
		return writeField(tarFld, value, waitMSs);
	}

	public boolean writeField(String fldName, String value, int waitMSs)
	{
		String msgOut = GWClient.buildSimpleMsg
		(
			"CQCGW:WriteField", "CQCGW:Field", fldName, "CQCGW:Value", value
		);
		return sendCmd(msgOut, waitMSs);
	}

	
    // -----------------------------------------------------------------------
    //	Methods the derived activity classes have to implement
    // -----------------------------------------------------------------------
    protected abstract int getLayoutResourceId();

    
    // -----------------------------------------------------------------------
    //	These methods handle the bgn thread that keeps the field data up to
    //	date and works across activity changes.
    // -----------------------------------------------------------------------
    
    //
    //	All this stuff below here is related to the static thread that manages
    //	our background connection and does polling and all that.
    //
    static private class GWThread extends Thread
    {
    	@Override public void run()
    	{
    		//
    		//	And now let's just loop until asked to shutdown. We use a
    		//	simple state machine to try to keep connected.
    		//
    		int iCounter = 0;
    		stopFlag = false;
    		curState = ThreadStates.WaitConfig;
    		while(!stopFlag)
    		{
    			int sleepTime = 0;
    			switch(curState)
    			{
    				case WaitConfig :
    					//
    					//	See if we have connection config info. We have to
    					//	sync here since the home activity will have to set
    					//	multiple values.
    					//
    					//	If not yet, then sleep a while.
    					//
    			    	synchronized(lockSync)
    			    	{    					
    			    		if (!userName.isEmpty())
    	    					curState = ThreadStates.WaitConnect;
    			    		else
    			    			sleepTime = 2000;
    			    	}
    					break;

    				case WaitConnect :
        				try
        				{
        					gwClient.connect(userName, password, hostAddr, hostPort);
        					Vector<String> newList = new Vector<String>();
        					gwClient.queryConfiguredRooms(newList);
        					
        					// It worked, so lock and update our info
        			    	synchronized(lockSync)
        			    	{
        			    		roomList = newList;
        			    		curState = ThreadStates.WaitRoom;
        			    		roomSerNum++;
        			    	}
        					
        					// Zero the counter since the next state uses it
        					iCounter = 0;
        				}
        				
        				catch(RuntimeException e)
        				{
        					sleepTime = 5000;
        				}
    					break;

    				case WaitRoom :
    					//
    					//	If we don't already have a name, we have to wait
    					//	for the fgn thread to allow the user to set one,
    					//	else just  move forward.
    					//
    					if (roomName.isEmpty())
    					{
	    					sleepTime = 1000;
	
	    					iCounter++;
	    					if (iCounter > 20)
	    					{
	    						try
	    						{
	    							iCounter = 0;    							
	    							pingServer();
	    						}
	    						
	            				catch(RuntimeException e)
	            				{
	            					gwClient.disconnect();
	            					curState = ThreadStates.Disconn;
	            					sleepTime = 3000;
	            				}
	    					}
    					}
    					 else
    					{
    						 curState = ThreadStates.RoomInit;
    					}
    					break;

    				case RoomInit :
        				try
        				{
        					initRoom();
        					curState = ThreadStates.Ready;
        				}
        				
        				catch(RuntimeException e)
        				{
        					gwClient.disconnect();
        					curState = ThreadStates.Disconn;
        					sleepTime = 3000;
        				}
    					break;
    					
    				case Ready :
        				try
        				{
        					doPoll();
        					
        					// Sleep a little to throttle us
        					sleepTime = 3000;
        				}
        				
        				catch(RuntimeException e)
        				{
        					if (!gwClient.checkIsConnected())
        					{
        						//
        						//	in this case, we don't want to to do a 
        						//	full disconnect. We want to try to
        						//	reconnect again using the info we have.
        						//	So disconnect the client engine, then go
        						//	to wait connect state.
        						//
        						sleepTime = 3000;
        						gwClient.disconnect();
        						curState = ThreadStates.WaitConnect;
        					}
        				}
    					break;
    					
    				case Disconn :
    					//
    					//	Disconnect the client and go back to wait config,
    					//	so we start trying to reconnect again. We don't
    					//	clear the connection info or room name so we can
    					//	re connect again. We only clear that stuff if we
    					//	give up and start over.
    					//
    					try
    					{
    						gwClient.disconnect();
    						curState = ThreadStates.WaitConfig;
    						sleepTime = 2500;
    					}
    					catch(RuntimeException e)
    					{
    						curState = ThreadStates.WaitConfig;
    					}
    					break;
    			};

   			
    			//
    			// 	If asked, sleep for a while. Note that we will wait for
    			//	cmds to process while we sleep.
    			//
    			//  !!We want to do one cmd per round at least, if there are
    			//	any available, even if the sleep time is zero. So we
    			//	make sure the loop enters at least once, updating the
    			//	current time at the bottom. If the sleep time is zero,
    			//	we will just do a non-wait check for a command.
    			//
    			long curTime = SystemClock.elapsedRealtime();
				final long endTime = curTime + sleepTime;
    			while(curTime <= endTime)
    			{
    				GWCmdInfo newCmd = null;
    				try
    				{
    					newCmd = cmdQ.poll
						(
							endTime - curTime, TimeUnit.MILLISECONDS
						);
    				}
    				
    				catch(InterruptedException e)
    				{
    					break;
    				}

    				if (newCmd != null)
    					processCmd(newCmd);
    				
    				curTime = SystemClock.elapsedRealtime();
    			}
    			
    			//
    			//	If the thread stop time stamp is non-zero, then see if we have 
    			//	gone past it. If so, then stop ourself. We have to lock here
    			//	to avoid a run condition with the fgn thread activity thread
    			//	doing on create/destroy processing.
    			//
    			if (thrStopTime != 0)
    			{
    				synchronized(lockSync)
    				{
    					// Check again now that we are locked, just in case
    					if (thrStopTime != 0)
    					{
	    					long tmp = SystemClock.elapsedRealtime(); 
	    					if (tmp > thrStopTime)
	    					{
	    						stopFlag = true;
	    						thrStopTime = 0;
	    					}
    					}
    				}
    			}
    		}

    		// 	Make sure we are disconnected and cleaned up.
    		try
    		{
    			stopFlag = false;
				thrStopTime = 0;    			
    			gwClient.disconnect();
    			
				userName = new String();
				password = new String();
				hostAddr = new String();
				hostPort = kDefPort;
				roomName = new String();
    			
   				curState = ThreadStates.WaitConfig;
    		}

    		catch(RuntimeException e)
    		{
    		}
    	}
    	

    	//
    	//	Called from our thread to do a poll of the fields we need to
    	//	keep track of. And to process any queued up commands from the
    	//	foreground.
    	//
    	private void doPoll()
    	{
    		//
    		//	See if we have any queued up commands from the foreground to
    		//	process. If so, do those.
    		//
  			gwClient.pollFields(changedFlds);
    			
  			// If we got any changes, update our field list
    		if (!changedFlds.isEmpty())
    			processFldChanges(changedFlds);
    	}

    	
    	//
    	//	Normally, when we are connected, we are polling fields
    	//	regularly so we always keep the server aware that we are
    	//	interested in keeping the connection alive (he'll drop a
    	//	connection if no) activity for a minute or so.) 
    	//
    	//	While waiting for a room selection from the user, we have
    	//	connected but haven't got any fields yet. So this is called
    	//	once in a while to keep the connection alive.
    	//
    	private void pingServer()
    	{
    		gwClient.ping();
    	}

    	
    	//
    	//	If the bgn thread sees a queued up command, this is called to
    	//	process it, in the context of the bgn thread so no locking needed
    	//	unless we need to store data.
    	//
    	private void processCmd(GWCmdInfo toProc)
    	{
    		try
    		{
	    		switch(toProc.cmdToDo)
	    		{
	    			case Disconnect :
	    				synchronized(lockSync)
	    				{
	    					//
	    					//	If we are already past the point where we are 
	    					//	waiting to connect, set it to disconnect, so that
	    					//	we'll disconnect and then start reconnecting.
	    					//
	    					//	Else, we just set it back to wait config so it'll
	    					//	start waiting for the front end to set login 
	    					//	info again.
	    					//
	    					if (curState.ordinal() >= ThreadStates.WaitConnect.ordinal())
	    						curState = ThreadStates.Disconn;
	    					else
	    						curState = ThreadStates.WaitConfig;
	    					
	    					// Clear the connection info
	    		    		hostPort = kDefPort;
	    		    		hostAddr = new String();
	    		    		password = new String();
	    		    		userName = new String();
	    		    		
	    		    		// And the room selection
	    		    		roomName = new String();
	    				}
	    				break;
	
	    			case SendQuery :
	    				//
	    				// 	Call a helper to do this. He will return the correct
	    				//	result object which will be the root element of
	    				//	the reply.
	    				//
	    				toProc.cmdRes = handleQuery(toProc.cmdText, toProc.cmdText2); 
	    				break;
	    				
	    			case SendCmd :
	    				// Send the command msg and wait for an ack
	    				gwClient.sendMsg(toProc.cmdText);
	    				gwClient.waitAck();
	    				break;
	
					default :
						break;
	    		};
    		}
    		
    		catch(Exception e)
    		{
    			// Make sure it gets posted, and put the error in
    			toProc.cmdRes = new GWCmdRes(e);
    		}
    		
    		// Post it as done
    		try
    		{
    			synchronized(lockSync)
    			{
    				toProc.notify();
    			}
    		}
    		catch(Exception e)
    		{
    			if (e != null)
    			{
    				
    			}
    		}
    	}

    	
    	//
    	//	We process changed fields here by storing way the values and marking
    	//	them as changed so the the foreground will see the changes when it
    	//	comes in to check for changes.
    	//
    	//	We have to sync here since we share the field data with the fgn.
    	//
    	private void processFldChanges(Vector<FldPollStatus> changedFlds)
    	{
        	synchronized(lockSync)
        	{
        		final int changeCnt = changedFlds.size();
        		for (int changeInd = 0; changeInd < changeCnt; changeInd++)
        		{
        			FldPollStatus curInfo = changedFlds.get(changeInd);

        			// Get the index of this one in the original poll list 
        			final int fldIndex = curInfo.getIndex();
        			
        			// And use that to either store the new value or error state
        			if (curInfo.getIsValid())
        				fldList.get(fldIndex).setValue(curInfo.getValue());
        			else
        				fldList.get(fldIndex).setErrState();
        		}
        		
        		//
        		// 	If any changes, bump the field serial number. Handle
        		//	wrap around to zero and move up to 1 if so.
        		//
        		if (changeCnt != 0)
        		{
        			fldSerialNum++;
        			if (fldSerialNum == 0)
        				fldSerialNum++;
        		}
        	}
    	}

    	
    	//
    	//	The foreground threads can queue up commands to be processed.
    	//	One of them is to send a query and get the reply. He sends us
    	//	the msg to send. We send it and get the reply, and return that
    	//	to the caller. If we get an exception, he will get back an error
    	//	instead when this is caught above by the command processor
    	//	method.
    	//
    	private GWCmdRes handleQuery(String msg, String expRep)
    	{
    		gwClient.sendMsg(msg);
    		Document docRet = gwClient.readMsg();

    		//
    		// 	Make sure it's the right type and get the root element. If
    		//	they set the expected type to null, then this will only do
    		//	basic checking that it's an XMLGW msg.
    		//
    		return new GWCmdRes(GWClient.checkMsgType(docRet, expRep, null));
    	}
    	

    	//
    	//	After we connect and then init, we go into room selection mode
    	//	where we are waiting for the fgn thread to let the user select
    	//	a room. This is called to query the info for the current room
    	//	and get it stored away and get the list of required fields set
    	//	into the poll list for us to start polling.
    	//
		private void initRoom()
		{
			//
			// 	Query the info for this room. If it fails, we just stay where
			//	we are, unless we lose connection. So we don't store anything
			//	until we have gotten the room config reply back.
			//
			Element roomCfg = null;
			try
			{
				String qMsg = GWClient.buildSimpleMsg("CQCGW:RoomCfgReq", "CQCGW:Name", roomName);
				gwClient.sendMsg(qMsg);
				
				// Get the reply
				Document cfgDoc = gwClient.readMsg();
				
				// Check that it's what we expect
				roomCfg = GWClient.checkMsgType(cfgDoc, "CQCGW:RoomCfgRep", null);
				
				// And it should contain a Room element
				roomCfg = GWClient.findFirstNamedElem(roomCfg, "CQCSysCfg:Room", false);
				if (roomCfg == null)
					return;
			}

			catch(CIDExcept cide)
			{
				// It won't be any sort of disconnect, just a protocol error or something
				return;
			}

			// Lock while we update
			synchronized(lockSync)
			{
				// Reset all the room data 
				initRoomData();
					
				// Store the overall room level info away
				storeRoomInfo(roomCfg);
			
				// Go through and process the info on the sub-sections
				Element msgElem = null;
				{
					Node curNode = roomCfg.getFirstChild();
					while (curNode != null)
					{
						if (curNode.getNodeType() == Node.ELEMENT_NODE)
						{
							msgElem = (Element)curNode;
	
							// Look for the elements we are interested in
							if (msgElem.getNodeName().equals("CQCSysCfg:Media"))
							{
								if (optMedia != CtrlOpt.NotUsed)
									storeMediaInfo(msgElem);
							}
							else if (msgElem.getNodeName().equals("CQCSysCfg:Security"))
							{
								if (optSecurity != CtrlOpt.NotUsed)
									storeSecInfo(msgElem);
							}
							else if (msgElem.getNodeName().equals("CQCSysCfg:Weather"))
							{
								//
								// 	Store the moniker of the weather driver and load up
								//	the fields we care about, if weather is enabled.
								//
								if (optWeather != CtrlOpt.NotUsed)
									storeWeatherInfo(msgElem);
							}
							else if (msgElem.getNodeName().equals("CQCSysCfg:Loads"))
							{
								// Go through these guys and store the info
								storeLoads(msgElem);
							}
						}
						curNode = curNode.getNextSibling();
					}
				}
			}

			//
			//	Go back through all of the various objects that we stored the
			//	data in, and ask them for their fields. We'll add those to our
			//	poll list and set up the index map appropriately.
			//
			//	We also set up our list of field info objects at the same time,
			//	since it needs to be the same order, and needs the same info.
			//
			Vector<String> fldPollList = new Vector<String>();
			for (int index = 0; index < loadList.size(); index++)
			{
				LoadInfo curLoad = loadList.get(index);
			
				if (curLoad.fld1Index != FldInfo.FldIndices.FldCount)
					addFieldToLists(curLoad.fld1Name, fldPollList, curLoad.fld1Index); 

				if (curLoad.fld2Index != FldInfo.FldIndices.FldCount)
					addFieldToLists(curLoad.fld2Name, fldPollList, curLoad.fld2Index);
			}

			//
			// 	Do any that aren't contained in a larger config class. Start
			//	with the non-forecast weather fields. They are all based off 
			//	the weather moniker. If there isn't one, then weather is not
			//	configured.
			//
			if (monWeather != null)
			{
				// Current conditions
				addFieldToLists(monWeather + ".CurBaro", fldPollList, FldInfo.FldIndices.Weath_CurBaro);
				addFieldToLists(monWeather + ".CurCondText", fldPollList, FldInfo.FldIndices.Weath_CurCondText);
				addFieldToLists(monWeather + ".CurHumidity", fldPollList, FldInfo.FldIndices.Weath_CurHumidity);
				addFieldToLists(monWeather + ".CurIcon", fldPollList, FldInfo.FldIndices.Weath_CurIcon);
				addFieldToLists(monWeather + ".CurTemp", fldPollList, FldInfo.FldIndices.Weath_CurTemp);
				addFieldToLists(monWeather + ".CurWindSpeed", fldPollList, FldInfo.FldIndices.Weath_CurWindSpeed);
				addFieldToLists(monWeather + ".FCCurrent", fldPollList, FldInfo.FldIndices.Weath_CurFCText);

				// Day 1 forecast
				addFieldToLists(monWeather + ".Day1CondText", fldPollList, FldInfo.FldIndices.Weath_FCD1_CondText);
				addFieldToLists(monWeather + ".Day1Stamp", fldPollList, FldInfo.FldIndices.Weath_FCD1_Stamp);
				addFieldToLists(monWeather + ".Day1Icon", fldPollList, FldInfo.FldIndices.Weath_FCD1_Icon);
				addFieldToLists(monWeather + ".Day1High", fldPollList, FldInfo.FldIndices.Weath_FCD1_High);
				addFieldToLists(monWeather + ".Day1Low", fldPollList, FldInfo.FldIndices.Weath_FCD1_Low);
				
				// Day 2 forecast
				addFieldToLists(monWeather + ".Day2CondText", fldPollList, FldInfo.FldIndices.Weath_FCD2_CondText);
				addFieldToLists(monWeather + ".Day2Stamp", fldPollList, FldInfo.FldIndices.Weath_FCD2_Stamp);
				addFieldToLists(monWeather + ".Day2Icon", fldPollList, FldInfo.FldIndices.Weath_FCD2_Icon);
				addFieldToLists(monWeather + ".Day2High", fldPollList, FldInfo.FldIndices.Weath_FCD2_High);
				addFieldToLists(monWeather + ".Day2Low", fldPollList, FldInfo.FldIndices.Weath_FCD2_Low);
				
				// Day 3 forecast
				addFieldToLists(monWeather + ".Day3CondText", fldPollList, FldInfo.FldIndices.Weath_FCD3_CondText);
				addFieldToLists(monWeather + ".Day3Stamp", fldPollList, FldInfo.FldIndices.Weath_FCD3_Stamp);
				addFieldToLists(monWeather + ".Day3Icon", fldPollList, FldInfo.FldIndices.Weath_FCD3_Icon);
				addFieldToLists(monWeather + ".Day3High", fldPollList, FldInfo.FldIndices.Weath_FCD3_High);
				addFieldToLists(monWeather + ".Day3Low", fldPollList, FldInfo.FldIndices.Weath_FCD3_Low);				
			}
			
			// Set the poll list
			gwClient.setPollList(fldPollList);
		}

		
		//
		//	This is a helper to initialize all of our room configuration based
		//	data. We have to do this on startup and any time the room changes,
		//	before we go back and fill it back in, so we want to break it out
		//	into a separate method.
		//
		//	The name of the currently selected room itself isn't reset here.
	    //
		private void initRoomData()
		{
			fldSerialNum = 1;
			
			// 	Clean out our lists to be reloaded
			changedFlds.removeAllElements();
			loadList.removeAllElements();
			fldList.removeAllElements();
			
			//
			//	Initialize the field index map to all -1's, so that any we
			//	don't set up will remain -1, which means nothing available.
			//
			for (int index = 0; index < FldInfo.FldIndices.FldCount.ordinal(); index++)
				fldIndexMap[index] = -1;
		}


		//
		//	A helper to set up a field into the field index map, the field
		//	poll list, and the field value list. The poll list and field info
		//	list slots will be at the same index, so the index map can be used
		//	to go to either.
		//
		private void addFieldToLists(String	name
									, Vector<String> pollList
									, FldInfo.FldIndices mapIndex)
		{
			//
			//	Set up the index map at the field index to map to the slot in
			//	the poll list, which is the current poll list size.
			//
			fldIndexMap[mapIndex.ordinal()] = pollList.size();
			
			// Add the field name to the poll list 
			pollList.add(name);

			// And add a new field info object for this field
			fldList.add(new FldInfo(name));
		}
		
		
		// Stores load oriented room config data
		private void storeLoads(Element msgElem)
		{
			// Go through all of the child elements
			Node curNode = msgElem.getFirstChild();
			int dimMax;
			int dimMin;
			String loadTypeStr;
			String fld1;
			String fld2;
			String loadName;
			String minVal, maxVal;
			LoadInfo.LoadTypes loadType;
			while (curNode != null)
			{
				if (curNode.getNodeType() == Node.ELEMENT_NODE)
				{
					msgElem = (Element)curNode;
					GWClient.checkNodeType(msgElem, "CQCSysCfg:Load");
					
					// Get the load type, which tells us what to expect
					loadTypeStr = msgElem.getAttribute("CQCSysCfg:Type");
					if (loadTypeStr.equals("Sw"))
						loadType = LoadInfo.LoadTypes.Switch;
					else if (loadTypeStr.equals("Dim"))
						loadType = LoadInfo.LoadTypes.Dimmer;
					else if (loadTypeStr.equals("Both"))
						loadType = LoadInfo.LoadTypes.Both;
					else
						loadType = LoadInfo.LoadTypes.NotUsed;
					
					//
					//	See if we have a field 2. If so, then this is a
					//	combo of switch and dimmer. Else it's either a
					//	dimmer or switch.
					//
					loadName = msgElem.getAttribute("CQCSysCfg:Name");
					fld1 = msgElem.getAttribute("CQCSysCfg:Fld");
					// If it's both, then we have a secondary switch field
					if (loadType == LoadInfo.LoadTypes.Both)
						fld2 = msgElem.getAttribute("CQCSysCfg:Fld2");
					else
						fld2 = null;

					// If field1 is a dimmer, then get the min/max if available
					dimMin = 0;
					dimMax = 100;
					if ((loadType == LoadInfo.LoadTypes.Dimmer)
					||  (loadType == LoadInfo.LoadTypes.Both))
					{
						try
						{
							minVal = msgElem.getAttribute("CQCSysCfg:Min");
							maxVal = msgElem.getAttribute("CQCSysCfg:Max");
							if (!minVal.isEmpty() && !maxVal.isEmpty())
							{
								dimMin = GenHelpers.textToInt(minVal, 0);
								dimMax = GenHelpers.textToInt(maxVal, 1);
							}
						}
						
						catch(RuntimeException e)
						{
							dimMin = 0;
							dimMax = 100;
						}
					}

					loadList.add
					(
						new LoadInfo
						(
							loadName
							, loadType
							, fld1
							, fld2
							, dimMin
							, dimMax
							, loadList.size()
						)
					);
				}
				curNode = curNode.getNextSibling();
			}
		}

		
		// Store media info
		private void storeMediaInfo(Element mediaInfo)
		{
			//
			// 	We have potentially repo and renderer monikers for both music
			//	and movies.
			//
			monMovieRepo = mediaInfo.getAttribute("CQCSysCfg:MovRep");
			monMusicRepo = mediaInfo.getAttribute("CQCSysCfg:MusRep");

			monMovieRend = mediaInfo.getAttribute("CQCSysCfg:MovPl");
			monMusicRend = mediaInfo.getAttribute("CQCSysCfg:MusPl");
		}

		
		// Store overall room info
		private void storeRoomInfo(Element roomCfg)
		{
			// Get the control options out
			if (roomCfg.getAttribute("CQCSysCfg:MedOpt").equals("ECtrlOpt_NotUsed"))
				optMedia = CtrlOpt.NotUsed;
			else if (roomCfg.getAttribute("CQCSysCfg:MedOpt").equals("ECtrlOpt_Display"))
				optMedia = CtrlOpt.Display;
			else if (roomCfg.getAttribute("CQCSysCfg:MedOpt").equals("ECtrlOpt_Control"))
				optMedia = CtrlOpt.Control;
			
			if (roomCfg.getAttribute("CQCSysCfg:SecOpt").equals("ECtrlOpt_NotUsed"))
				optSecurity = CtrlOpt.NotUsed;
			else if (roomCfg.getAttribute("CQCSysCfg:SecOpt").equals("ECtrlOpt_Display"))
				optSecurity = CtrlOpt.Display;
			else if (roomCfg.getAttribute("CQCSysCfg:SecOpt").equals("ECtrlOpt_Control"))
				optSecurity = CtrlOpt.Control;
			
			if (roomCfg.getAttribute("CQCSysCfg:WeatOpt").equals("ECtrlOpt_NotUsed"))
				optWeather = CtrlOpt.NotUsed;
			else if (roomCfg.getAttribute("CQCSysCfg:WeatOpt").equals("ECtrlOpt_Display"))
				optWeather = CtrlOpt.Display;
			else if (roomCfg.getAttribute("CQCSysCfg:WeatOpt").equals("ECtrlOpt_Control"))
				optWeather = CtrlOpt.Control;
		}

		
		// Stores security related room config info
		private void storeSecInfo(Element msgElem)
		{
			// If security isn't configured, clear the moniker and return
			if (optSecurity == CtrlOpt.NotUsed)
			{
				monSecurity = null;
				return;
			}
			// Store the security panel moniker
			monSecurity = msgElem.getAttribute("CQCSysCfg:Panel");

/*			
			// Store the security area and room zone count
			secArea = msgElem.getAttribute("CQCSysCfg:SecArea");
			secZoneCnt = GWClient.textToInt(msgElem.getAttribute("CQCSysCfg:ZoneCnt"));

			// The zone fields are a comma separate list
			String[] zoneList = msgElem.getTextContent().split(",");
			int zCnt = zoneList.length;

			for (int index = 0; index < zCnt; index++)
			{

			}
*/			
		}


		// STores the weather related room config info
		private void storeWeatherInfo(Element msgElem)
		{
			// If weather isn't configured, clear the moniker and return
			if (optWeather == CtrlOpt.NotUsed)
			{
				monWeather = null;
				return;
			}
			
			// Else get the info we need
			monWeather = msgElem.getAttribute("CQCSysCfg:Drv");
		}
    }


    //
    // 	We have a couple buttons in the standard header that we handle at this
    //	level.
    //
    private Button.OnClickListener hdrButtonHandler = new Button.OnClickListener()
    {  
		public void onClick(View v)
		{
			Intent intent = new Intent
			(
				getApplicationContext(), LoginActivity.class
			);
			startActivity(intent);
		}
    };

    //
    //	Our handler that calls the derived activity's update timer callback
    //	also calls this one, so that we have a chance to update stuff that
    //	we handle for all of them.
    //
    private Runnable updateTimer = new Runnable()
    {
    	public void run()
    	{
    		//
    		//	If our last thread state we saw isn't the same as the
    		//	last one we saw, update the connected/disconneced image
    		//	in the home button. We have to lock and grab a copy of
    		//	the current state first.
    		//
    		ThreadStates compState;
    		synchronized(lockSync)
    		{
    			compState = curState;
    		}
    		
    		if (lastTimerThrState.ordinal() != compState.ordinal())
    		{
    			lastTimerThrState = compState;

    			Drawable toDraw;
    			if (compState.ordinal() == ThreadStates.Ready.ordinal())
    				toDraw = getResources().getDrawable(R.drawable.connected);
    			else
    				toDraw = getResources().getDrawable(R.drawable.disconn);
    			((ImageButton)findViewById(R.id.HomeButton)).setImageDrawable(toDraw);
    		}
    	}
    };
    
    // -----------------------------------------------------------------------
    //	Private data
    // -----------------------------------------------------------------------

    // An object used for sync of field data access and to the work queue
    private static Object lockSync;

    //
    //	All our activities derive from this one. We reference count those
    //	activities using onCreate() and onDestroy(). When the count goes
    //	zero we set a time stamp a bit forward in time, and the bgn thread
    //	will stop on its own when that time is hit (if another activity 
    //	doesn't start up in the meantime and clear the time stamp.)
    //
    private static int activityRef;
    private static long thrStopTime;
    
    //
    // 	The foreground update timer and the target to call. Derived activities
    //	provide a callback for us to call. We have a paused flag that we set
    //	and clear in onPause()/onResume, to stop the timer while we are
    //	paused.
    //
    private Timer fgnTimer;
    private Runnable fgnTimerTar;
    private boolean timerPaused;

    //
    //	We need a last thread state we saw for use by the update timer,
    //	separate from the bgn thread who has his own. This one isn't
    //	static, it's per-derived activity, so we can know it changed.
    //
    private ThreadStates lastTimerThrState;

    //
    //	This is where we store the fields. The thread keeps these updated.
    //	We store the last the serial number the server gives us so we can
    //	pass it back in later to see if there are any changes that the server
    //	needs to tell us about.
    //
    //	We have an array that provides a link between the enumerated field
    //	index and into a vector of field info objects that reflect the
    //	poll list we set on the GW server.
    //
    private static int fldIndexMap[];
    private static Vector<FldInfo> fldList;
    private static int fldSerialNum;

    //
    //	We also need a vector of poll status values to use when polling the
    //	GW server, which gets filled with changes. It's flushed and reloaded
    //	on each query.
    //
    private static Vector<FldPollStatus> changedFlds;
    
    //
    //	These are the classes that hold the info related to room config.
    //
    //	loadList - 	One load info object for each load configured for the
    //				current room. Only valid if the thread state is Ready.
    //	monXXX   -  Devce monikers that are defined in the room config
    //  optXXX   -  The option selection for those major sections of room
    //				config that can be enabled or not.
    //	roomList -  The list of configured room names. This is only set if
    //				the thread state indicates we are at least waiting for
    //				a room selection.
    //	roomName -	The name of the currently selected room. Only valid if
    //				the thread state is Ready.
    //	roomSerNum- Each time we load the room list, this is bumped so that
    //				the derived actions can know if they have out of date
    //				room info. If their last gotten serial number is not
    //				the same as this, then either we've lost and reconnected
    //				or a new room was selected.
    //
    private static Vector<LoadInfo> loadList;
    private static CtrlOpt optMedia;
    private static CtrlOpt optSecurity;
    private static CtrlOpt optWeather;
    private static String monMovieRend;
    private static String monMovieRepo;
    private static String monMusicRend;
    private static String monMusicRepo;
    private static String monSecurity;
    private static String monWeather;
    private static Vector<String> roomList;
    private static String roomName;
    private static int roomSerNum;

    //
    //	The derived classes make calls on us to interact with the GW server.
    //	The bgn thread has to process them, so they are queued up. The bgn
    //	thread grabs them and processes them, then posts them to let the
    //	waiting fgn thread wait for them.
    //
    private static LinkedBlockingQueue<GWCmdInfo> cmdQ;
    
    //
    // 	The home activity gets the connection info from the user. It stores
    //	entered info and brings it back next time as defaults.
    //
    private static String userName;
    private static String password;
    private static String hostAddr;
    private static int hostPort;
    
    //
    // 	Our thread and it's current state machine state, and GW client object,
    //	and a breakout flag to ask it to stop.
    //
    private static ThreadStates curState;
    private static GWThread gwThread;  
    private static GWClient gwClient;
    private static boolean stopFlag;
}
