package com.charmedquark.cqcandroidc;

import java.util.Vector;

import org.w3c.dom.Element;

import android.content.Intent;
import android.os.Bundle;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.charmedquark.xmlgwclient.*;

public class MBrowseActivity extends GWActivity
{
	// -----------------------------------------------------------------------
	//	Public class types
	// -----------------------------------------------------------------------
	
	//
	// 	We can handle either music or movie mode and switch back and forth if
	//	both are available. We use this to remember which we are in. Though
	//	it should happen, if there should be none enabled (which should not
	//	be the case if media is enabled), then we can set none.
	//
	enum EModes { None, Music, Movie };

	//
	//	We display multiple levels of information. For music it is:
	//
	//		Categories -> Title Sets -> Collections -> Items
	//
	//	For movies the Items level isn't used.
	//
	//	So we need to remember which level we are on. Each level is just a
	//	list of text values.
	//
	enum ELevels { Cats, Titles, Collects, Items };

	
	public MBrowseActivity()
	{

	}


	// -----------------------------------------------------------------------
	//	Methods we have to override from our common activity base class
    // -----------------------------------------------------------------------
	@Override
	protected int getLayoutResourceId()
	{
		return R.layout.activity_mbrowse;
	}


	// -----------------------------------------------------------------------
	//	Lifecycle methods we have to handle
	// -----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        // Make sure we get an initial thread state transition
        lastThreadState = null;

        // And make sure we get new room config initially
	    lastRoomSerNum = 0;
	    
	    // We are initially not in a download
	    inDownload = false;

	    // Initialize our mode and level and repos
	    curMode = EModes.None;
	    curLevel = ELevels.Cats;
	    repoMovie = null;
	    repoMusic = null;
	    
	    //
	    // 	Allocate our list of lists, and put the lists into it
	    //	We need one per level.
	    //
	    listList = new Vector<Vector<KeyValuePair>>();
	    listList.add(new Vector<KeyValuePair>());
	    listList.add(new Vector<KeyValuePair>());
	    listList.add(new Vector<KeyValuePair>());
	    listList.add(new Vector<KeyValuePair>());

	    // Allocate and initialize our level indices array
	    listIndices = new int[4];
	    listIndices[0] = -1;
	    listIndices[1] = -1;
	    listIndices[2] = -1;
	    listIndices[3] = -1;
	    
	    // Look up our views that we keep track of
	    backButton = (Button)findViewById(R.id.BackButton);
	    listPref = (TextView)findViewById(R.id.ListPref);
	    mediaType = (Button)findViewById(R.id.MediaType);
	    progBar = (ProgressBar)findViewById(R.id.DnloadProg);
	    viewList = (ListView)findViewById(R.id.MediaList);
	    
	    // Set the adaptor on the list. Just a simple one is fine
	    listAdapter = new ArrayAdapter<String>
    	(
    		this, R.layout.listlayout_med_text, R.id.text1
		);
	    viewList.setAdapter(listAdapter);

	    //
		// 	And the item click handler for the room list, and the buttons
	    //	handler on the buttons.
	    //
		backButton.setOnClickListener(buttonHandler);
		mediaType.setOnClickListener(buttonHandler);
		viewList.setOnItemClickListener(listClickHandler);

		// Initially hide the progress bar
		progBar.setVisibility(View.INVISIBLE);
		
    	//
    	//	In this case, we don't do an initial refresh here. We just let it
		//	happen in the first timer update.
    	//
        enableUpdateTimer(updateTimer);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    }

    @Override
    protected void onStop()
    {
       	super.onStop();
    }

    @Override
    protected void onPause()
    {
    	super.onPause();
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    }

    
	// -----------------------------------------------------------------------
    // 	An update timer we register with our parent class. It'll get
    //	called periodically so we can update status info.
	// -----------------------------------------------------------------------
    private Runnable updateTimer = new Runnable()
    {
    	public void run()
    	{
    		// Do any updating required
    		refreshView();
    	}
    };

  
    
	// -----------------------------------------------------------------------
    //	Private helper methods
	// -----------------------------------------------------------------------

    //
    //	We use a number of async tasks, all of which share some common
    //	functionality. So we create our own base class that the others
    //	will extend.
    //
    private abstract class MBrwsAsync extends AsyncTask<Integer, Integer, Boolean>
    {
    	public Exception failErr = null;
    	
    	@Override
		protected void onPreExecute()
		{
			inDownload = true;
			progBar.setVisibility(View.VISIBLE);
		}
		
    	@Override
    	protected void onPostExecute(Boolean res)
    	{
    		inDownload = false;    
    		progBar.setVisibility(View.INVISIBLE);
    	}
    }


    //
    // 	Builds up a media cursor browse request msg. We request them in up
    //	to 64 at a time. That's a reasonable size since it will not create
    //	enormous transfers but won't make for too many transfers either.
    //
    private String
    buildBrowseReq(String tarRepo, String cursorId, int curCnt)
    {
    	StringBuilder sbTar = new StringBuilder();
		GWClient.fmtMsgHeader(sbTar);

		sbTar.append("   <CQCGW:BrowseReq CQCGW:Moniker=\"");
		sbTar.append(tarRepo);
		sbTar.append("\" CQCGW:CursorId=\"");
		sbTar.append(cursorId);
		sbTar.append("\" CQCGW:Index=\"");
		sbTar.append(curCnt);
		sbTar.append("\" CQCGW:PageSz=\"64\"/>");
		
		GWClient.fmtMsgFooter(sbTar);		
    	return sbTar.toString();
    }
    
    
    //
    // 	Closes a media repo cursor. Make it fault tolerant since we call it
    //	in exception handlers to insure that cursors get closed.
    //
    private void closeCursor(String cursorId)
    {
    	try
    	{
	    	String msgOut = GWClient.buildSimpleMsg
			(
				"CQCGW:CursorCloseReq"
				, "CQCGW:Moniker", getRepo()
				, "CQCGW:CursorId", cursorId
			);
	    	sendCmd(msgOut, 4000);
    	}
    	
    	catch(Exception e)
    	{
    		// Log something here
    	}
    }
    
    //
    // 	Query the media category list for the current media type. It updates
    //	the first list in our list of lists, the category level one, and 
    //	loads it into the list view. This is done via an async task since
    //	it can take a while.
    //
    //	All the widgets are disabled until it completes, then it will re-
    //	enable them as appropriate.
    //
    private class GetCatList extends MBrwsAsync
    {
		//
		// 	The input parameters aren't used in this case, since there's no
		//	previous selection driving this one. This is the first list in
		//	the series.
		//
		@Override
		protected Boolean doInBackground(Integer... parms)
    	{
			Vector<KeyValuePair> tarList = listList.get(ELevels.Cats.ordinal());
			tarList.removeAllElements();
			try
			{
				// Build up the query msg to send
				String msgOut = GWClient.buildSimpleMsg
				(
					"CQCGW:CatListReq"
					, "CQCGW:Moniker", getRepo()
					, "CQCGW:MType", getType()
					, "CQCGW:NoEmpty", "True"
				);
		
				// Queue up the query and get the result
				GWCmdRes qRes = sendQuery(msgOut, "CQCGW:CatListRep");
		
				// If it fails, the result is an exception
				if (!qRes.cmdStatus)
					throw(qRes.err);
		
				// Parse out the values we want into our target k/v pair list.
				GWClient.parseKVLists
				(
					qRes.resMsg, tarList, "CQCGW:Names", "CQCGW:Cookies", null
				);
			}
			
			catch(Exception e)
			{
				failErr = e;
				return false;
			}
			return true;
		}

		@Override
    	protected void onPostExecute(Boolean res)
    	{
			super.onPostExecute(res);    		
    		if (res)
    		{
    			Vector<KeyValuePair> tarList = listList.get(ELevels.Cats.ordinal());
    			loadListView(tarList, 0);
       		 	setListPrefix();
    		}
    		 else
    		{
     			showMsg("Failed to get category list", failErr.getMessage());	
    		}
    	}
    }

    // Return the appropriate player moniker for the current media mode
    private String getPlayer()
    {
		if (curMode == EModes.Music)
			return rendMusic;
		return rendMovie;
    }
    
    // Return the appropriate repo for the current media mode
    private String getRepo()
    {
		if (curMode == EModes.Music)
			return repoMusic;
		return repoMovie;
    }

    // Return the appropriate media type for the current media mode
    private String getType()
    {
		if (curMode == EModes.Music)
			return "EMediaType_Music";
		return "EMediaType_Movie";
    }

    
    //
    //	When we are on category level, and a selection is made, we load up
    //	the titles in that collection, load those into the list, move our
    //	level up to title level, and store the index of the category we
    //	were on, so we can go back to it if they move up to the category
    //	level again.
    //
    //	We create a cursor, iterate the whole thing, then close the
    //	cursor again.
    //
    //	As with the other queries, it's done as an async task.
    //
    private class LoadCatTitles extends MBrwsAsync
    {
		protected Boolean doInBackground(Integer... parms)
    	{
			//
			// 	Get the passed in index of the category selected, and then get
			//	the KV pair for that category. And the repository for our
			//	current mode.
			//
			final int selIndex = parms[0];
			final KeyValuePair selCat = listList.get(0).get(selIndex);
			final String tarRepo = getRepo(); 
    	
			String cursorId = null;
			try
			{
				// Build up the cursor request msgs
				String msgOut = GWClient.buildSimpleMsg
				(
					"CQCGW:CursorReq"
					, "CQCGW:Moniker", tarRepo
					, "CQCGW:Cookie", selCat.getValue()
					, "CQCGW:Order", "ESortOrder_Title"
				);
	
				// Queue up the query and get the result
				GWCmdRes qRes = sendQuery(msgOut, "CQCGW:CursorRep");
				if (!qRes.cmdStatus)
					throw(qRes.err);
				
				Element rootElem = qRes.resMsg;
				cursorId = rootElem.getAttribute("CQCGW:CursorId");
				
				//
				//	We also get the full count, which tells us how many titles we
				//	have to load. At this point we catch errors so we can release
				//	the cursor id even if we fail.
				//
				final int titleCnt = GenHelpers.textToInt
				(
					rootElem.getAttribute("CQCGW:Count"), 0
				);
	
				//	
				// 	Loop and request title info in reasonable sized chunks. Load the
				//	results up into a temp list until we are sure we have it.
				//
				Vector<KeyValuePair> newList = new Vector<KeyValuePair>();
				int curCnt = 0;
				while (curCnt < titleCnt)
				{
					msgOut = buildBrowseReq(tarRepo, cursorId, curCnt);
					
					// Send the query and get the response
					qRes = sendQuery(msgOut, "CQCGW:BrowseRep");
					if (!qRes.cmdStatus)
						throw(qRes.err);
					
					//
					// 	Pull this set of values out and add them to the list,
					//	and add the number parsed out to our current count. 
					//
					curCnt += GWClient.parseKVLists
					(
						qRes.resMsg
						, newList
						, "CQCGW:Titles"
						, "CQCGW:Cookies"
						, "CQCGW:Artists"
					);
				}
	
				// OK, it worked, so store the new info and display it
				listIndices[curLevel.ordinal()] = selIndex;
				
				curLevel = ELevels.Titles;			
				listList.set(curLevel.ordinal(), newList);
	
				// Close the cursor now that we are done with it
				closeCursor(cursorId);
			}
	
			catch(Exception e)
			{
				// Close the cursor if it was created
				if (cursorId != null)
					closeCursor(cursorId);
				
				failErr = e;
				return false;
			}
			return true;
    	}

    	protected void onPostExecute(Boolean res)
    	{
    		super.onPostExecute(res);    		
    		if (res)
    		{
    			Vector<KeyValuePair> tarList = listList.get(ELevels.Titles.ordinal());
    			loadListView(tarList, 0);
       		 	setListPrefix();
    		}
    		 else
    		{
    			 showMsg("Failed to get list of titles", failErr.getMessage());	
    		}
    	}
    }

    
    //
    //	This is called when a collection is clicked on. If we are in
    //	movie mode, this won't be called. The collection will be treated
    //	as the user selection. For music, we have to display the items
    //	in the selected collection. This is not done via a cursor like
    //	the titles and collections. There's a specific query for this.
    //
    //	As with the other queries, it's an async task
    //
    private class LoadColItems extends MBrwsAsync
    {
		protected Boolean doInBackground(Integer... parms)
    	{
			//
			// 	Get the passed in index of the category selected, and then get
			//	the KV pair for that category. And the repository for our
			//	current mode.
			//
			final int selIndex = parms[0];
			final KeyValuePair selCol = listList.get(2).get(selIndex);
			final String tarRepo = getRepo(); 

			try
			{
	   			String msgOut = GWClient.buildSimpleMsg
				(
					"CQCGW:ItemListReq"
					, "CQCGW:Moniker", tarRepo
					, "CQCGW:ColCookie", selCol.getValue()
				);
	
				// Queue up the query and get the result
				GWCmdRes qRes = sendQuery(msgOut, "CQCGW:ItemListRep");
				if (!qRes.cmdStatus)
					throw(qRes.err);
	
				Vector<KeyValuePair> newList = new Vector<KeyValuePair>();
				GWClient.parseKVLists
				(
					qRes.resMsg
					, newList
					, "CQCGW:Names"
					, "CQCGW:Cookies"
					, "CQCGW:Artists"
				);
				
				// OK, it worked, so store the new info and display it
				listIndices[curLevel.ordinal()] = selIndex;
				
				curLevel = ELevels.Items;			
				listList.set(curLevel.ordinal(), newList);
			}
			
			catch(Exception e)
			{
				failErr = e;
				return false;
			}
			return true;
    	}

    	protected void onPostExecute(Boolean res)
    	{
    		super.onPostExecute(res);
    		if (res)
    		{
    			Vector<KeyValuePair> tarList = listList.get(ELevels.Items.ordinal());
    			loadListView(tarList, 0);
       		 	setListPrefix();
    		}
	   		 else
	   		{
	   			 showMsg("Failed to get list of items", failErr.getMessage());	
	   		}    		
    	}
	}

    
    //
    //	Given one of our lists, load it into the list view. The key
    //	of the key/value pair is always the display value. We also
    //	will select the indicated item, and make it visible. When
    //	going back up to a previous level, we re-select the previously
    //	selected item at that level.
    //
    private void loadListView(Vector<KeyValuePair> toLoad, int toSel)
    {
    	listAdapter.clear();
		final int itemCnt = toLoad.size();
		for (int index = 0; index < itemCnt; index++)
			listAdapter.add(toLoad.get(index).getKey());

		//
		// 	If we got a non-zero selection index, try to select that guy
		//	and make it visible.
		//
		if (toSel != 0)
			viewList.setSelection(toSel);
    }


    //
    //	When we are on title level, and a selection is made, we load up the
    //	collections in that title set, load those into the list, move our
    //	level up to collection level, and store the index of the title set we
    //	were on, so we can go back to it if they move up to the title set
    //	level again.
    //
    //	As with the other queries, this is done as an async task.
    //
    private class LoadTitleCols extends MBrwsAsync
    {
		protected Boolean doInBackground(Integer... parms)
    	{
			//
			// 	Get the passed in index of the title selected, and then get
			//	the KV pair for that title. And the repository for our
			//	current mode.
			//
			final int selIndex = parms[0];
			final KeyValuePair selTitle = listList.get(1).get(selIndex);
			final String tarRepo = getRepo(); 

			String cursorId = null;
			try
			{
				// Build up the cursor request msgs
				String msgOut = GWClient.buildSimpleMsg
				(
					"CQCGW:CursorReq"
					, "CQCGW:Moniker", tarRepo
					, "CQCGW:Cookie", selTitle.getValue()
					, "CQCGW:Order", "ESortOrder_Title"
				);
	
				// Queue up the query and get the result
				GWCmdRes qRes = sendQuery(msgOut, "CQCGW:CursorRep");
				if (!qRes.cmdStatus)
					throw(qRes.err);
	
				//	Get the root element from the document, and the cursor id we need.
				Element rootElem = qRes.resMsg;
				cursorId = rootElem.getAttribute("CQCGW:CursorId");
				
				//
				//	We also get the full count, which tells us how many titles we
				//	have to load. At this point we catch errors so we can release
				//	the cursor id even if we fail.
				//
				final int titleCnt = GenHelpers.textToInt
				(
					rootElem.getAttribute("CQCGW:Count"), 0
				);
	
				//	
				// 	Loop and request title info in reasonable sized chunks. Load the
				//	results up into a temp list until we are sure we have it.
				//
				Vector<KeyValuePair> newList = new Vector<KeyValuePair>();
				int curCnt = 0;
				while (curCnt < titleCnt)
				{
					msgOut = buildBrowseReq(tarRepo, cursorId, curCnt);
					
					// Send the query and get the response
					qRes = sendQuery(msgOut, "CQCGW:BrowseRep");
					if (!qRes.cmdStatus)
						throw(qRes.err);
					
					//
					// 	Pull this set of values out and add them to the list,
					//	and add the number parsed out to our current count. 
					//
					curCnt += GWClient.parseKVLists
					(
						qRes.resMsg
						, newList
						, "CQCGW:Titles"
						, "CQCGW:Cookies"
						, "CQCGW:Artists"
					);
				}
	
				//
				// 	OK, it worked, so store the new info and display it. Store the
				//	selected index at this level before we change the level.
				//
				listIndices[curLevel.ordinal()] = selIndex;
				
				// Now move up and store the new list for the new level
				curLevel = ELevels.Collects;
				listList.set(curLevel.ordinal(), newList);
	
				// Close the cursor now that we are done with it
				closeCursor(cursorId);
			}
	
			catch(Exception e)
			{
				// Close the cursor if we got it opened
				if (cursorId != null)
					closeCursor(cursorId);
	
				failErr = e;
				return false;
			}
			return true;
	    }

	
    	protected void onPostExecute(Boolean res)
    	{
    		super.onPostExecute(res);
    		
    		if (res)
    		{
    			Vector<KeyValuePair> tarList = listList.get(ELevels.Collects.ordinal());
    			loadListView(tarList, 0);
       		 	setListPrefix();
    		}
	   		 else
	   		{
	   			 showMsg("Failed to get list of collections", failErr.getMessage());	
	   		}    		
    	}
    }

    
    //
    //	A single music item was selected. We just queue it up on the target
    //	player. It shouldn't be possible to get here if the renderer moniker
    //	is null, check to be sure.
    //
    private void playItem(KeyValuePair selTitle)
    {
    	if (rendMusic == null)
    	{
    		// Log something and display error
    		return;
    	}
    	
    	final String tarPlayer = getPlayer();
    	writeField(tarPlayer, "EnqueueMedia", selTitle.getValue(), 5000);
    }

    
    //
    //	When in movie mode, when the user clicks on a collection, we take that
    //	as desire to preview that movie, possibly in preparation for playing
    //	it.
    //
    //	We get the selected movie collection item, and can use that to query
    //	the collection details for display.
    //
    private class PreviewMovie extends MBrwsAsync
    {
		// We store the retrieved info here for the completion method to see 
    	String colCookie = null;
    	String dirName = null;
    	String titleName = null;
    	String relYear = null;

		protected Boolean doInBackground(Integer... parms)
    	{
			final int selIndex = parms[0];
			final KeyValuePair selCol = listList.get(2).get(selIndex);
			final String tarRepo = getRepo();

	    	try
	    	{
	    		// Let's try to get the details on this guy
				String msgOut = GWClient.buildSimpleMsg
				(
					"CQCGW:ColDetReq"
					, "CQCGW:Moniker", tarRepo
					, "CQCGW:ColCookie", selCol.getValue()
				);
	
				// Queue up the query and get the result
				GWCmdRes qRes = sendQuery(msgOut, "CQCGW:ColDetRep");
				if (!qRes.cmdStatus)
					throw(qRes.err);
	
				//	Get the root element from the document, and the cursor id we need.
				Element rootElem = qRes.resMsg;
	
				//
				// 	Pull out all of the values and store them locally so
				//	that our completion method can see them.
				//
				dirName = rootElem.getAttribute("CQCGW:Artist"); 
				relYear = rootElem.getAttribute("CQCGW:Year");

				// Some we already have
				colCookie = selCol.getValue();
				titleName = selCol.getKey();
	    	}
	    	
	    	catch(Exception e)
	    	{
				failErr = e;
				return false;
		    }
	    	return true;
    	}

		
    	protected void onPostExecute(Boolean res)
    	{
    		super.onPostExecute(res);
    		if (res)
    		{
    			//
    			//	Set up an intent for the movie preview activity,
    			//	and set the values on it that we want to pass
    			//	along to it.
    			//
				Intent intent = new Intent(getApplicationContext(), MoviePreviewActivity.class);
				intent.putExtra("ColCookie", colCookie);
				intent.putExtra("Director", dirName);
				intent.putExtra("Repo", getRepo());
				intent.putExtra("TitleName", titleName);
				intent.putExtra("Year", relYear);
				startActivity(intent);
    		}
	   		 else
	   		{
	   			 showMsg("Failed to get title details", failErr.getMessage());	
	   		}
    	}
    };
    
    
    //
    //	This is called upon creation, if our timer sees that the room
    //	config has changed, or when we are started up again. We see if the
    //	info we have is still valid. If not, we update ourself.
    //
    private void refreshView()
    {
    	//
    	//	Get the current state of the bgn thread, and the latest room
    	//	config serial number.
    	//
    	final ThreadStates curState = getState();
    	final int curSerNum = getRoomSerialNum();

    	// If either the thread state or the serial number has changed, see what's up
    	if ((lastThreadState == null)
    	||  (lastThreadState != curState)
    	||  (curSerNum != lastRoomSerNum))
    	{
    		// We can update the thread state either way
    		lastThreadState = curState;
    		
    		//
    		//	If we either are not ready, or we have no room config, we have to
    		//	just disable everything and do nothing.
    		//
    		if ((curState != ThreadStates.Ready) || (curSerNum == -1))
    		{
    			//
	    		// 	At this point, update the last serial number now. Even if it's
    			//	-1, that's fine, that insures we see a change if it goes to
    			//	anything that indicates we have content.
    			//
    			//	We don't want to store it otherwise, since we want to see
    			//	the change below and update. 
    			//
    			lastRoomSerNum = curSerNum;

    			//	Reset ourself and return
    			resetMediaState(EModes.None);
	    		return;
	    	}
    	}
    	
    	//
    	// 	See if our room configuration has changed. The code above handled
    	//	the situation where it was -1 (meaning none available, so we know
    	//	we have some data here.
    	//
    	if (getRoomSerialNum() != lastRoomSerNum)
    	{
    		lastRoomSerNum = getRoomSerialNum();
    		
    		// Get the new repo monikers
    		repoMovie = getMonMovieRepo();
    		repoMusic = getMonMusicRepo();

    		// Get the new player monikers
    		rendMovie = getMonMovieRend();
    		rendMusic = getMonMusicRend();

    		
    		// Set an initial mode, preferring music if available
    		EModes newMode = EModes.Music;
    		if (repoMusic == null)
    		{
    			if (repoMovie == null)
    				newMode = EModes.None;
    			else
    				newMode = EModes.Movie;
    		}
    		
    		// Reset for whatever mode we got initially
   			resetMediaState(newMode);
    	}        
    }


    //
    // 	Called to get us reset, either back to an empty content state because
    //	we have no room info, or to a new media mode.
    //
    private void resetMediaState(EModes newMode)
    {
    	curLevel = ELevels.Cats;
    	for (int index = 0; index < 4; index++)
    	{
    		listIndices[index] = -1;
    		Vector<KeyValuePair> curList = listList.get(index);
    		if (curList != null)
    			curList.removeAllElements();
    	}
    	
    	//
    	// 	If no new mode, then clear the monikers and the list. If a new
    	//	mode, load up the category list.
    	//
    	curMode = newMode;
    	if (curMode == EModes.None)
    	{
    		repoMovie = null;
    		repoMusic = null;
    		rendMovie = null;
    		rendMusic = null;

			listAdapter.clear();
    	}
    	 else
    	{
    		 // Get the cat list for the new mode and load the view with it
    		 new GetCatList().execute();
    	}
    	
    	// Update the media mode button either way
    	setMediaTypeButton();
    }

    
    //
    // 	Given the current level, set the list prefix text to indicate what we
    //	are showing in the list.
    //
    private void setListPrefix()
    {
    	String toLoad = null;
    	switch(curLevel)
    	{
    		case Cats :
    			toLoad = getString(R.string.pref_catlist);
    			break;
    			
    		case Titles :
    			toLoad = getString(R.string.pref_titlelist);
    			break;
    			
    		case Collects :
    			toLoad = getString(R.string.pref_collist);
    			break;
    			
    		case Items :
    			toLoad = getString(R.string.pref_itemlist);
    			break;
    	};

    	//
    	// 	If not at cats level, get the name of the things we are in.
    	//	Else, we use the media mode name.
    	//
    	String parName = "";
    	if (curLevel != ELevels.Cats)
    	{
    		//
    		//	These are in the general form "Title: Bubba's Game", so
    		//	we show the prefix text from above plus the name of the
    		//	thing that was selected in the upstream list to get to the
    		//	current list.
    		//
    		final int prevLev = curLevel.ordinal() - 1;
    		int parIndex = listIndices[prevLev];
    		parName = listList.get(prevLev).get(parIndex).getKey();
    	}
    	 else if (curMode == EModes.Movie)
    	{
    		 parName = "Movies";
    	}
    	 else if (curMode == EModes.Music)
    	{
    		 parName = "Music";
    	}
    	
    	// And finally set the text
    	listPref.setText(toLoad + " " + parName);
    }
    
    
    //
    //	Sets the text on the media selection button. We set it to the opposite
    //	of what the current mode is, since clicking on it will select the new
    //	mode.
    //
    private void setMediaTypeButton()
    {
		if (curMode == EModes.Music)
			mediaType.setText(getString(R.string.media_Movies));
		else if (curMode == EModes.Movie)
			mediaType.setText(getString(R.string.media_Music));
		else
			mediaType.setText("");
    }
    
	// -----------------------------------------------------------------------
    //	Handlers for user interaction
	// -----------------------------------------------------------------------
    
    // A handler for our login button 	
    private Button.OnClickListener buttonHandler = new Button.OnClickListener()
    {  
		public void onClick(View v)
		{
			// Ignore if in a download
			if (inDownload)
				return;
			
			// Depending on the button pressed, take the appropriate action
			if (v.getId() == R.id.MediaType)
			{
				// Whatever the media type is now, flip it if possible
				if (((curMode == EModes.Movie) && (repoMusic != null))
				||  ((curMode == EModes.Music) && (repoMovie != null))) 
				{
					EModes newMode;
					if (curMode == EModes.Movie)
						newMode = EModes.Music;
					else
						newMode = EModes.Movie;
					
					// Reset for this new media type
					resetMediaState(newMode);
				}
			}
			 else if (v.getId() == R.id.BackButton)
			{
				 // If not at the category level move up
				 if (curLevel != ELevels.Cats)
				 {
					 // Trash the current list
					 listList.set(curLevel.ordinal(), null);

					 // Calc the new level
					 int newLevel = curLevel.ordinal() - 1;
					 curLevel = ELevels.values()[newLevel];

					 //
					 //	And load this guy up, and select the previously
					 //	selected index.
					 //
		    		 loadListView
		    		 (
	    				 listList.get(curLevel.ordinal())
	    				 , listIndices[curLevel.ordinal()]
    				 );
		    		 setListPrefix();
				 }
			}
		}
	};

    
    // The list selection handler
    private OnItemClickListener listClickHandler = new OnItemClickListener()
    {
        public void
        onItemClick(AdapterView<?> adapter, View tarView, int position, long id)
        {
        	// If we aren't in WaitRoom state or beyond, then we don't do anything
        	if (getState() != ThreadStates.Ready)
        		return;

			// Ignore if in a download
			if (inDownload)
				return;        	
        	
        	//
        	//	Depending on the current level we are displaying, we react to
        	//	the thing clicked on and either open a new level of list, or
        	//	invoke the preview activity. Get the list for our current display
        	//	level, and then out of that list get the key/value pair for the
        	//	selected item.
        	//
        	KeyValuePair curSel = listList.get(curLevel.ordinal()).get(position);
        	
        	switch(curLevel)
        	{
        		case Cats :
        			// We want to get the titles of this category and load them
        			new LoadCatTitles().execute(position);
        			break;

        		case Titles :
        			// We want to get the collections in this title and load them
        			new LoadTitleCols().execute(position);
        			break;

        		case Collects :
        			//
        			//	In music mode we load up the items from the selected 
        			//	collection. In movie mode, we want to move on to the
        			//	preview screen.
        			//
        			if (curMode == EModes.Music)
        				new LoadColItems().execute(position);
        			else
        				new PreviewMovie().execute(position);
        			break;

        		case Items :
        			//
        			//	If we get here, we are in music mode, and they invoked
        			//	a single item. we just add it to the player's playlist.
        			//
        			playItem(curSel);
        			break;
        			
    			default :
    				break;
        	};
        }
    };
    

    
	// -----------------------------------------------------------------------
    //	Data members
	// -----------------------------------------------------------------------
    
    // The last thread state we saw, so we know if it transitions
    private ThreadStates		lastThreadState;

    //
    //	We download lists asynchronously. So we need to prevent the user from
    //	doing anything while the list is being downloaded. This flag is set by
    //	each of the async task objects when it starts and cleared when they
    //	complete. Our input handlers check it and do nothing if it's set.
    //
    private boolean 			inDownload;
    
    //
    //	We remember the last room serial number we got. We can check this
    //	periodically and see if it's changed. If so, we need to reconfigure
    //	ourself since the config info has changed.
    //
    private int					lastRoomSerNum;
    
    //
    //	The media movie and music repo monikers we last got from the room
    //	config. Either may be empty, since they may do only music or only
    //	music in a given room.
    //
    private String				repoMovie;
    private String				repoMusic;

    //
    //	The media movie and music player monikers we last got from the room
    //	config. Either may be empty, since they may do only music or only
    //	music in a given room.
    //
    private String				rendMovie;
    private String				rendMusic;    
    
    //
    //	We can be switched between music and movie mode, if both monikers
    //	are configured. And our currently display level. 
    //
    private EModes				curMode;
    private ELevels				curLevel;

    //
    // 	Our target list that we load stuff into, and its adapter which we
    //	have to access often.
    //
    private ListView 			viewList;
    private ArrayAdapter<String> listAdapter;

    // Other views we keep up with
    private Button				backButton;
    private TextView			listPref;
    private Button				mediaType;
    private ProgressBar			progBar;
    
    //
    //	Our lists that we store current info in. There is one for each
    //	possible level. Each one holds a list of key/value pair objects
    //	so that we can track two or three bits of info per level. The
    //	key is always the display value.
    //
    private Vector<Vector<KeyValuePair>> listList;
    
    //
    //	As we drill downwards into the lists, we want to remember the
    //	selection in the previous lists that got us there, so that we can
    //	reselect those if the user asks to go back. So we need one index
    //	per level. For those not used, we store -1.
    //
    private int listIndices[];
}
